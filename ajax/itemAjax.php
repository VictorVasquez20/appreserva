<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once '../modelos/Item.php';

$item = new Item();

$iditem = isset($_POST['iditem']) ? $_POST['iditem'] : 0;
$nombre = strtoupper(isset($_POST['nombre']) ? $_POST['nombre'] : ""); 
$color = isset($_POST['color']) ? $_POST['color'] : ""; 
$vigencia = isset($_POST['vigencia']) ? $_POST['vigencia'] : "";


switch ($_GET["op"]) {
    case 'guardaryeditar':
        if(empty($iditem)){
            $rspta = $item->insertar($nombre, $color, $vigencia);
            echo $rspta ? "Item insertado" : "Item no insertado";
        }else{
            $rspta = $item->editar($iditem, $nombre, $color, $vigencia);
            echo $rspta ? "Item editado" : "Item no editado";
        }
        break;
        
    case 'selectitem':
        $rspta2 = $item->selectItem();
        echo '<option value="" selected disabled>Seleccione item</option>';
        
        while($reg = $rspta2->fetch_object()){
            echo '<option value='.$reg->iditem.'>'.$reg->nombre.'</option>';
            /*if($reg->iditem == $iditem){
                echo '<option value='.$reg->iditem.' selected="selected">'.$reg->nombre.'</option>';
            }else{
                echo '<option value='.$reg->iditem.'>'.$reg->nombre.'</option>';
            }*/
        }
        break;
    case 'selectitemCERO':
        $rspta2 = $item->selectItemCERO();
        echo '<option value="" selected disabled>Seleccione item</option>';
        
        while($reg = $rspta2->fetch_object()){
            echo '<option value='.$reg->iditem.'>'.$reg->nombre.'</option>';
            /*if($reg->iditem == $iditem){
                echo '<option value='.$reg->iditem.' selected="selected">'.$reg->nombre.'</option>';
            }else{
                echo '<option value='.$reg->iditem.'>'.$reg->nombre.'</option>';
            }*/
        }
        break;
    case 'listar':
        
        $rspta = $item->listar();
        
        $data = Array();
        while ($reg = $rspta->fetch_object()){
            if($reg->vigencia == 0) {
                $cond = "<span class='label label-danger'>no</span>"; 
            }else{
                $cond = "<span class='label label-success'>si</span>";
            }
            $data[] = array(
                    "0"=>
                    '<button class="btn btn-warning btn-xs" onclick="mostrar('.$reg->iditem.')"><i class="fa fa-pencil"></i></button>',
                    "1"=>$reg->iditem,
                    "2"=>$reg->nombre,
                    "3"=>$reg->color,
                    "4"=>$cond
            );
        }
        $results = array(
                        "sEcho"=>1,
                        "iTotalRecords"=>count($data),
                        "iTotalDisplayRecords"=>count($data), 
                        "aaData"=>$data
                );

        echo json_encode($results);
        break;
    case 'mostrar':
        $rspta = $item->mostrar($iditem);
        echo json_encode($rspta);
        break;
}