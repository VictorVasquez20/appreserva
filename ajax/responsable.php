<?php
session_start();

require_once '../modelos/Responsable.php';

$cat = new Responsable();

$idresponsable = isset($_POST['idresponsable']) ? $_POST['idresponsable'] : "";
$nombre = isset($_POST['nombre']) ? $_POST['nombre'] : "";
$emailEmpresa = isset($_POST['emailEmpresa']) ? $_POST['emailEmpresa'] : "";
$condicion = isset($_POST['condicion']) ? $_POST['condicion'] : 0;

switch ($_GET['op']) {
    case 'guardaryeditar':

        if (!$idresponsable) {
            $rspta = $cat->Insertar($nombre, $emailEmpresa, $condicion);
            echo $rspta;
        } else {
            $rspta = $cat->Editar($idresponsable, $nombre, $emailEmpresa, $condicion);
            echo $rspta;
        }

        break;

    case 'listar':
        $rspta = $cat->Listar();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => '<button class="btn btn-warning btn-xs" onclick="mostrar(' . $reg->idresponsable . ')"><i class="fa fa-pencil"></i></button>',
                "1" => $reg->nombre,
                "2" => $reg->emailEmpresa,
                "3" => ($reg->condicion) ? '<span class="label bg-green">Activo</span>' : '<span class="label bg-red">No activo</span>'
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;

    case 'mostrar':
        $rspta = $cat->Mostrar($idresponsable);
        echo json_encode($rspta);
        break;

    case 'selectresponsable':
        $rspta = $cat->SelectCategoria();
        echo '<option selected disabled>Seleccione un responsable</option>';
        while ($reg = $rspta->fetch_object()) {
            echo '<option value=' . $reg->idresponsable . '>' . $reg->nombre . '</option>';
        }
        break;
}
