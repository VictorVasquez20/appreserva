<?php
require_once "../modelos/Usuario.php";
require_once "../modelos/Reserva.php";
require_once "../modelos/Item.php";

$respuesta = array();

if (isset($_GET['api'])) {
    
    switch ($_GET['api']) {
        
        case 'iniciosession':
            $usuario= new Usuario();
            if((isset($_GET['username']) && $_GET['username'] != "") && (isset($_GET['password']) && $_GET['password'] != "")){
                $username = $_GET['username'];
                $password = $_GET['password'];
                $password_hash = hash("SHA256", $password);
                $rspta=$usuario->verificar($username, $password_hash);
                $fecth = $rspta->fetch_object();
                
                if(count($fecth) > 0){
                    $respuesta['error'] = false;
                    $respuesta['message'] = 'solicitud completada con exito';
                    $respuesta['contenido'] = $fecth;      
                }else{
                    $respuesta['error'] = true;
                    $respuesta['message'] = 'Usuario o contrasena incorrectos.';
                }
            }else{
                $respuesta['error'] = true;
                $respuesta['message'] = 'No es posible realizar esta peticion ';
            }
            break;
            
        case 'listareservas':
            $reserva = new Reserva();
            $rspta = $reserva->listarTodoDashboard();
            $data = Array();
            while ($reg = $rspta->fetch_object()) {
                $data[] = array(
                    "idreserva" => $reg->idreserva,
                    "item" => $reg->item,
                    "desde" => $reg->desde . " " . $reg->horadesde,
                    "hasta" => $reg->hasta . " " . $reg->horahasta,
                    "nombitem" => $reg->nombItem,
                    "color" => $reg->color,
                    "horadesde" => $reg->horadesde,
                    "descripcion" => $reg->descripcion,
                    "solicitante" => $reg->solicitante,
                    "nomb" => $reg->nomb,
                    "estado" => $reg->estado,
                    "comentario" => is_null($reg->comentario) ? "Sin comentarios." : $reg->comentario,
                    "centrocosto" => $reg->centrocosto,
                    "idresponsable" => $reg->idresponsable,
                    "nombresponsable" => $reg->responsable
                );
            }
            $respuesta['error'] = false;
            $respuesta['message'] = 'solicitud completada con exito';
            $respuesta['contenido'] = $data;
            break;
            
        case 'listaritems':
            $item = new Item();
            $rspta = $item->selectItemCERO();
            
            $data = Array();
            while ($reg = $rspta->fetch_object()){
                $data[] = array(
                    "codigo" => $reg->iditem, 
                    "nombre"=>$reg->nombre
                );
            }
            $respuesta['error'] = false;
            $respuesta['message'] = 'solicitud completada con exito';
            $respuesta['contenido'] = $data;
            break;
            
        Case 'guardarreserva':
            $reserva = new Reserva();
            
            $idreserva = isset($_GET['idreserva']) ? $_GET['idreserva'] : "";
            $iduser = isset($_GET['iduser']) ? $_GET['iduser'] : $_GET['iduser'];
            $item = isset($_GET['item']) ? $_GET['item'] : "";
            $desde = isset($_GET['desde']) ? $_GET['desde'] : "";
            $hasta = isset($_GET['hasta']) ? $_GET['hasta'] : "";
            $horadesde = isset($_GET['horadesde']) ? $_GET['horadesde'] : "";
            $horahasta = isset($_GET['horahasta']) ? $_GET['horahasta'] : "";
            $descripcion = strtoupper(isset($_GET['descripcion']) ? $_GET['descripcion'] : "");
            $nombre = isset($_GET['nombre']) ? $_GET['nombre'] : "";
            $solicita = isset($_GET["solicita"]) ? $_GET["solicita"] : $_SESSION['idempleado'];
            $estado = isset($_GET["estado"]) ? $_GET["estado"] : 0;
            $comentario = strtoupper(isset($_GET["comentario"]) ? $_GET["comentario"] : "");
            $centrocosto = isset($_GET["centrocosto"]) ? $_GET["centrocosto"] : "";
            $responsable = isset($_GET["responsable"]) ? $_GET["responsable"] : 0;
            
            $var  = (count(json_decode($_GET))); //$_GET['iduser']. ",".$_GET['item'].",".$_GET['horadesde'];
            
            if(!$idreserva || $idreserva == 0){
                $rspta = $reserva->insertar($iduser, $item, $desde, $hasta, $horadesde, $horahasta, $descripcion, $nombre, $solicita, $estado, $comentario, $centrocosto, $responsable);
                
                if (!is_nan($rspta)) {
                    if (data != 0) {
                        $respuesta['error'] = false;
                        $respuesta['message'] = 'solicitud completada con exito '.$rspta;
                        $respuesta['contenido'] = $rspta;
                    }else{
                        $respuesta['error'] = true;
                        $respuesta['message'] = 'Error al insertar reserva '. $rspta . $var;
                    }
                }else{
                    $respuesta['error'] = true;
                    $respuesta['message'] = $rspta;
                }
            }
            break;
    }
} else {
    //Valida que venga una llamada correcta a la APi
    $respuesta['error'] = true;
    $respuesta['message'] = 'Llamda incorrecta a la API';
}
echo json_encode($respuesta);

