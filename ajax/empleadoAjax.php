<?php 
/*header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');*/

session_start();
require_once "../modelos/Empleado.php";
require_once "../modelos/OfiDepartamento.php";


$empleado = new Empleado();

$idempleado=isset($_POST["idempleado"])?limpiarCadena($_POST["idempleado"]):"";
$nombre=isset($_POST["nombre"])?limpiarCadena($_POST["nombre"]):"";
$apellido=isset($_POST["apellido"])?limpiarCadena($_POST["apellido"]):"";
$tipo_documento=isset($_POST["tipo_documento"])?limpiarCadena($_POST["tipo_documento"]):"";
$num_documento=isset($_POST["num_documento"])?limpiarCadena($_POST["num_documento"]):"";
$fecha_nac=isset($_POST["fecha_nac"])?limpiarCadena($_POST["fecha_nac"]):"";
$direccion=isset($_POST["direccion"])?limpiarCadena($_POST["direccion"]):"";
$movil=isset($_POST["movil"])?limpiarCadena($_POST["movil"]):"";
$residencial=isset($_POST["residencial"])?limpiarCadena($_POST["residencial"]):"";
$email=isset($_POST["email"])?limpiarCadena($_POST["email"]):"";
$idprovincias=isset($_POST["idprovincias"])?limpiarCadena($_POST["idprovincias"]):"";
$idregiones=isset($_POST["idregiones"])?limpiarCadena($_POST["idregiones"]):"";
$idcomunas=isset($_POST["idcomunas"])?limpiarCadena($_POST["idcomunas"]):"";
$idoficina=isset($_POST["idoficina"])?limpiarCadena($_POST["idoficina"]):"";
$iddepartamento=isset($_POST["iddepartamento"])?limpiarCadena($_POST["iddepartamento"]):"";
$idcargo=isset($_POST["idcargo"])?limpiarCadena($_POST["idcargo"]):"";

$fromdate = date("Y-m-d", strtotime($fecha_nac)); 


switch ($_GET["op"]) {
    case 'guardaryeditar':
            if(!file_exists($_FILES['imagen']['tmp_name']) || !is_uploaded_file($_FILES['imagen']['tmp_name'])){
                    $imagen=$_POST["imagenactual"];	
            }else{
                    $ext = explode(".",$_FILES['imagen']['name']);
                    if($_FILES['imagen']['type'] == "image/jpg" || $_FILES['imagen']['type'] == "image/jpeg" || $_FILES['imagen']['type'] == "image/png" ){
                            $imagen = round(microtime(true)).".".end($ext);
                            move_uploaded_file($_FILES['imagen']['tmp_name'], "../files/empleados/".$imagen);
                    }
            }
            $iduser=$_SESSION['iduser'];
            if(empty($idempleado)){
                    $ofidepartamento = new OfiDepartamento();
                    $idoficina_departamento=$ofidepartamento->id_ofidepartamento($idoficina, $iddepartamento);
                    $id=intval($idoficina_departamento["idoficina_departamento"]);

                    if(empty($id)){
                            $id=$ofidepartamento->insertar($idoficina, $iddepartamento);
                            $rspta=$empleado->insertar($nombre,$apellido,$tipo_documento,$num_documento,$fromdate,$direccion,$movil, $residencial,$email,$imagen, $iduser, $idcargo, $idcomunas, $idprovincias, $idregiones, $id);
                            echo $rspta ? "Usuario registrado" : "Usuario no pudo ser registrado";
                    }else{
                            $rspta=$empleado->insertar($nombre,$apellido,$tipo_documento,$num_documento,$fromdate,$direccion,$movil, $residencial,$email,$imagen, $iduser, $idcargo, $idcomunas, $idprovincias, $idregiones, $id);
                            echo $rspta ? "Usuario registrado" : "Usuario no pudo ser registrado";
                    }		
            }
            else{
                    $ofidepartamento = new OfiDepartamento();
                    $idoficina_departamento=$ofidepartamento->id_ofidepartamento($idoficina, $iddepartamento);
                    $id=intval($idoficina_departamento["idoficina_departamento"]);

                    if(empty($id)){
                            $id=$ofidepartamento->insertar($idoficina, $iddepartamento);
                            $rspta=$empleado->editar($idempleado,$nombre,$apellido,$tipo_documento,$num_documento,$fecha_nac,$direccion,$movil, $residencial,$email,$imagen, $iduser, $idcargo, $idcomunas, $idprovincias, $idregiones, $id);
                            echo $rspta ? "Usuario editado" : "Usuario no pudo ser editado";
                    }else{
                            $rspta=$empleado->editar($idempleado,$nombre,$apellido,$tipo_documento,$num_documento,$fecha_nac,$direccion,$movil, $residencial,$email,$imagen, $iduser, $idcargo, $idcomunas, $idprovincias, $idregiones, $id);
                            echo $rspta ? "Usuario editado" : "Usuario no pudo ser editado";
                    }
            }
            break;

    case 'guardar':
            $iduser=1;
                    $ofidepartamento = new OfiDepartamento();
                    $idoficina_departamento=$ofidepartamento->id_ofidepartamento($idoficina, $iddepartamento);
                    $id=intval($idoficina_departamento["idoficina_departamento"]);
                    if(empty($id)){
                            $id=$ofidepartamento->insertar($idoficina, $iddepartamento);
                            $rspta=$empleado->insertar($nombre,$apellido,$tipo_documento,$num_documento,$fromdate,$direccion,$movil, $residencial,$email,null, $iduser, $idcargo, $idcomunas, $idregiones, $id);
                            echo $rspta ? "Usuario registrado" : "Usuario no pudo ser registrado";
                    }else{
                            $rspta=$empleado->insertar($nombre,$apellido,$tipo_documento,$num_documento,$fromdate,$direccion,$movil, $residencial,$email,null, $iduser, $idcargo, $idcomunas, $idregiones, $id);
                            echo $rspta ? "Usuario registrado" : "Usuario no pudo ser registrado";
                    }		
            break;

    case 'desactivar':
            $rspta=$empleado->desactivar($idempleado);
                    echo $rspta ? "Empleado inhabilitado" : "Empleado no se pudo inhabilitar";
            break;

    case 'activar':
            $rspta=$empleado->activar($idempleado);
                    echo $rspta ? "Empleado habilitado" : "Empleado no se pudo habilitar";
            break;

    case 'mostar':
            $rspta=$empleado->mostrar($idempleado);
                    echo json_encode($rspta);
            break;

    case 'selectrut':

            $rut = $_POST["rut"];
            $fecth = NULL;

            $existe = $empleado->verificaempleado($rut);
            $fetch1 = $existe->fetch_object();
            $filas = $fetch1->numFila;

            if ($filas > 0)
            {
                $rspta=$empleado->selectEmpleadoRut($rut);

                $fecth = $rspta->fetch_object();

                if(isset($fecth)){
                        $_SESSION['idempleado']=$fecth->idempleado;
                        $_SESSION['nombre']=$fecth->nombre;
                        $_SESSION['apellido']=$fecth->apellido;
                        $_SESSION['movil']=$fecth->movil;
                        $_SESSION['imagen']= ($fecth->imagen == "" || $fecth->imagen == "null" ? "noimg.jpg" : $fecth->imagen);
                        $_SESSION['rut'] = $fecth->num_documento;
                        $_SESSION['administrador'] = 0;
                        $_SESSION['iduser'] = 0;

                    echo json_encode($fecth); 			

                }
            }else{
                echo json_encode($fecth);
            }
            break;

    case 'listar':
            $rspta=$empleado->listar();
            $data = Array();
            while ($reg = $rspta->fetch_object()){
                    $data[] = array(
                                    "0"=>($reg->condicion)?
                                    '<button class="btn btn-warning btn-xs" onclick="mostar('.$reg->idempleado.')"><i class="fa fa-pencil"></i></button>'.
                                    ' <button class="btn btn-danger btn-xs" onclick="desactivar('.$reg->idempleado.')"><i class="fa fa-close"></i></button>':
                                    '<button class="btn btn-warning btn-xs" onclick="mostar('.$reg->idempleado.')"><i class="fa fa-pencil"></i></button>'.
                                    ' <button class="btn btn-primary btn-xs" onclick="activar('.$reg->idempleado.')"><i class="fa fa-check"></i></button>',
                                    "1"=>$reg->nombre.' '.$reg->apellido,
                                    "2"=>$reg->tipo_documento.'-'.$reg->num_documento,
                                    "3"=>$reg->cargo,
                                    "4"=>$reg->departamento,
                                    "5"=>$reg->oficinas,
                                    "6"=>($reg->condicion)?'<span class="label bg-green">Habilitado</span>':'<span class="label bg-red">Inhabilitado</span>'
                            );
            }
            $results = array(
                            "sEcho"=>1,
                            "iTotalRecords"=>count($data),
                            "iTotalDisplayRecords"=>count($data), 
                            "aaData"=>$data
                    );

            echo json_encode($results);
            break;

    case 'listaroficina':
            $idoficina = $_GET["id"];
        $rspta=$empleado->listar_oficina($idoficina);
        $data = Array();
        while ($reg = $rspta->fetch_object()){
                $data[] = array(
                    "0"=>$reg->nombre.' '.$reg->apellido,
                    "1"=>$reg->tipo_documento.'-'.$reg->num_documento,
                    "2"=>$reg->departamento.' - '.$reg->cargo	        
                        );
        }
        $results = array(
            "iTotalRecords"=>count($data),
            "aaData"=>$data
                 );
        echo json_encode($results);
        break;

    case 'listardepartamento':
        $iddepartamento = $_GET["id"];
        $rspta=$empleado->listar_departamento($iddepartamento);
        $data = Array();
        while ($reg = $rspta->fetch_object()){
                $data[] = array(
                    "0"=>$reg->nombre.' '.$reg->apellido,
                    "1"=>$reg->tipo_documento.'-'.$reg->num_documento,
                    "2"=>$reg->oficinas.' - '.$reg->cargo	        
                        );
        }
        $results = array(
            "iTotalRecords"=>count($data),
            "aaData"=>$data
                 );
        echo json_encode($results);
        break;

    case 'listarcargo':
            $idcargo = $_GET["id"];
        $rspta=$empleado->listar_cargo($idcargo);
        $data = Array();
        while ($reg = $rspta->fetch_object()){
                $data[] = array(
                    "0"=>$reg->nombre.' '.$reg->apellido,
                    "1"=>$reg->tipo_documento.'-'.$reg->num_documento,
                    "2"=>$reg->oficinas.' - '.$reg->departamento	        
                        );
        }
        $results = array(
            "iTotalRecords"=>count($data),
            "aaData"=>$data
                 );
        echo json_encode($results);
        break;


    case 'selectempleado':
            $rspta = $empleado->selectempleado();
            echo '<option value="" selected disabled>Seleccione Empleado</option>';
            while($reg = $rspta->fetch_object()){
                echo '<option value='.$reg->idempleado.'>'.$reg->nombre.' '.$reg->apellido.' / '.$reg->num_documento.'</option>';
            }
        break;

    case 'salir':
            session_unset();
            session_destroy();
            header("Location: ../produccion/login.php");
            break;

}