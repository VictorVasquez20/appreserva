<!DOCTYPE html>
<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Servicios e Informatica</title>

    <link rel="icon" href="../public/favicon.ico" type="image/x-icon" />
    
    <!-- Bootstrap -->
    <link href="../public/build/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../public/build/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../public/build/css/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="../public/build/css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../public/build/css/custom.css" rel="stylesheet">
 
    <style type="text/css">
	.rut-error{
            color: #fff;
            font-weight: bold;
            background-color: red;
            padding: 2px 10px;
            display: inline-block;
            margin-left: 5px;
        }
    </style>
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
              <form method="post" id="frmAcceso" class="form-horizontal form-label-left input_mask" name="frmAcceso">
              <h5><b>SERVICIOS E INFORMATICA</b>
                  <hr>
                  <b>Reservas</b></h5>
                <div class="form-group has-feedback">
                    
                        
                    <input type="text" class="form-control has-feedback-left" data-inputmask="'mask' : '[*9.][999.999]-[*]'" id="rut" name="rut" placeholder="    Ingrese Rut" required="" />
                    <span class="fa fa-id-card form-control-feedback left" aria-hidden="true"></span>
                </div>
                
              <!--<div>
                <input type="password" class="form-control" id="password" name="password" placeholder="Password" required="" /> 
              </div>-->
              <div>
                <button class="btn btn-default" type="submit">Ingresar</button>
                
                <!--<a class="reset_pass" href="#">Olvidaste tu clave?</a>-->
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><img src="../files/fabrimetal.png" height="40" width="40"><!--<i class="fa fa-building "></i>-->Fabrimetal</h1>
                  <a href="loginAdmin.php">Portal Administrador</a>
                  <p>©2017 Fabrimetal sobre plantilla Bootstrap. Terminos y Privacidad</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>

    <!-- jQuery -->
    <script src="../public/build/js/jquery.min.js"></script>

    <!-- Bootstrap -->
    <script src="../public/build/js/bootstrap.min.js"></script>
    <!-- Bootbox Alert -->
    <script src="../public/build/js/bootbox.min.js"></script>
    <script src="../public/build/js/jquery.inputmask.bundle.min.js"></script>
    <script type="text/javascript" src="scripts/login.js"></script>
    
    
    <!--<script type="text/javascript">
        jQuery(document).ready(function($){
                $('.input_rut').rut();
        });
    </script>
    <!-- Custom Theme Scripts -->
    <script src="../public/build/js/custom.js"></script>

  </body>
</html>
