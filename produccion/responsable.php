<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {
    require 'header.php';
    if ($_SESSION['administrador'] == 1) {
        ?>

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>RESPONSABLES</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a id="op_agregar" onclick="mostrarform(true)">Agregar</a></li>
                                            <li><a id="op_listar" onclick="mostrarform(false)">Listar</a></li>
                                        </ul>
                                    </li>                     
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div id="listadoresponsable" class="x_content">

                                <table id="tblresponsable" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>NOMBRE</th>
                                            <th>EMAIL</th>
                                            <th>CONDICIÓN</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>

                            <div id="formularioresponsable" class="x_content">
                                <br />
                                <div class="col-md-12 center-margin">
                                    <form class="form-horizontal form-label-left" id="formulario" name="formulario">
                                        <div class="form-group">
                                            <label class="control-label col-md-2 col-sm-2 col-xs-12">NOMBRE</label>
                                            <div class="col-md-10 col-sm-10 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control" name="nombre" id="nombre">
                                                <input type="hidden" id="idresponsable" name="idresponsable" class="form-control">
                                            </div>
                                        </div>	
                                        <div class="form-group">
                                            <label class="control-label col-md-2 col-sm-2 col-xs-12">EMIAL</label>
                                            <div class="col-md-10 col-sm-10 col-xs-12 form-group has-feedback">
                                                <input type="text" class="form-control" name="emailEmpresa" id="emailEmpresa">
                                            </div>
                                        </div>	
                                        <div class="form-group">
                                            <label class="control-label col-md-2 col-sm-2 col-xs-12">CONDICIÓN</label>
                                            <div class="col-md-10 col-sm-10 col-xs-12 form-group has-feedback">
                                                <div class="">
                                                    <label>
                                                        <input name="condicion" id="condicion" type="checkbox" class="js-switch" value="1"/>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="ln_solid"></div> 
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelarform()">Cancelar</button>
                                                <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiar()">Limpiar</button>
                                                <button class="btn btn-success" type="submit" id="btnGuardar">Agregar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- /page content -->
        <?php
    } else {
        require 'nopermiso.php';
    }
    require 'footer.php';
    ?>
        <script type="text/javascript" src="scripts/responsable.js"></script>
    <?php
}
ob_end_flush();

