<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';
    ?>

    <div class="right_col" role="main">
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Calendario de Reservas </h2>
                        <div id="filtros" style="float: right" class="dt-buttons btn-group">
                            <!-- <p class="btn">FILTROS</p>
                            <a class="btn btn-default buttons-copy buttons-html5" onclick="listarEstado('1,2,3', 0);">Todos</a>
                            <a class="btn btn-default buttons-copy buttons-html5" style="color:#060;" onclick="listarEstado('1,0', 1);">Mis Agendados</a>
                            <a class="btn btn-default buttons-copy buttons-html5" style="color:#E74C3C;" onclick="listarEstado(2, 1);">Mis Rechazados</a> 
                            <a class="btn btn-default buttons-copy buttons-html5" style="color: #269abc;" onclick="listarEstado(3, 1);">Mis Anulados</a>  -->
                            <p class="btn">FILTROS</p>
                                <select id="filtros" class="form-control" onchange="calendario();">
                                    <option value="0,1,2,3">Todos</option>
                                    <option value="1">Agendados</option>
                                    <option value="2">Rechazados</option>
                                    <option value="3">Anulados</option>
                                </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div id='calendario2'></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="CalenderModalNew" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <form id="Solicitaform" class="form-horizontal calender" role="form" enctype="multipart/form-data">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">SOLICITUD DE RESERVA</h4>
                    </div>
                    <div class="modal-body">
                        <div id="testmodal" style="padding: 5px 20px;">

                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label class="control-label">ITEM</label>
                                    <select name="item" id="item" class="form-control selectpicker" required=""></select>
                                    <input type="hidden" id="solicita1" name="solicita1" value="<?php echo $_SESSION['idempleado'] ?>">
                                    <input type="hidden" id="fechadesde" name="fechadesde">
                                    <input type="hidden" id="fechahasta" name="fechahasta">
                                </div>
                                <div class="col-sm-6">
                                    <label class="control-label">TIPO</label>
                                    <select name="treserva" id="treserva" class="form-control selectpicker" required="">
                                        <option value="0">DESPACHO</option>
                                        <option value="1">RETIRO</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label class="control-label">CENTRO COSTO</label>
                                    <select name="centrocosto" id="centrocosto" class="form-control selectpicker" required="campo requerido" data-live-search="true"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div id="div_desde" class="col-sm-6">
                                    <label class="control-label">DESDE</label>
                                    <p id="fechadesde3"></p>
                                    <div class='input-group date' id='myDatepicker_desde'>
                                        <input type='text' name="desde" id="desde" class="form-control" placeholder="Seleccione Hora Desde" readonly  />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div id="div_hasta" class="col-sm-6">
                                    <label class="control-label">HASTA</label>
                                    <p id="fechahasta3"></p>
                                    <div class='input-group date' id='myDatepicker_hasta'>
                                        <input type='text' name="hasta" id="hasta" class="form-control" placeholder="Seleccione Hora Hasta" readonly />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">    
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label class="control-label">DIRECCIÓN</label>
                                        <input name="direccion" id="direccion" class="form-control selectpicker" required="" style="text-transform: uppercase;">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-sm-8">
                                    <label class="control-label">CONTACTO EN OBRA (Fabrimetal)</label>
                                    <input name="recibe" id="recibe" class="form-control selectpicker" required="" style="text-transform: uppercase;">
                                </div>

                                <div class="col-sm-4">
                                    <label class="control-label">TELEFONO</label>
                                    <input name="telefono" id="telefono" class="form-control selectpicker" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-8">
                                    <label class="control-label">CONTACTO DE OBRA</label>
                                    <input name="contactoobra" id="contactoobra" class="form-control selectpicker" required="" style="text-transform: uppercase;">
                                </div>

                                <div class="col-sm-4">
                                    <label class="control-label">TELEFONO</label>
                                    <input name="fonoobra" id="fonoobra" class="form-control selectpicker" required="">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label class="control-label">DESCRIPCIÓN</label>
                                    <textarea class="form-control" style="height:55px; text-transform: uppercase;" id="descr" name="descr" placeholder="DESCRIPCION DE RESERVA"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-9">
                                    <label class="control-label">ADJUNTAR ARCHIVO (excel)</label>
                                    <input type="file" id="excel" name="excel" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" class="custom-file-input" required="">
                                </div>
                                <div class="col-sm-3">
                                    <label class="control-label">FORMATO</label><br>
                                    <a href="../files/formatos/formato.xlsx" target="_blank"><img src="../files/icono/Excel-icon.png" height="30px" width="30px" ></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default antoclose" data-dismiss="modal">Cerrar</button>
                        <button type="submit" id="enviar" class="btn btn-primary">Guardar</button>
                    </div>

                </div>
            </form>
        </div>
    </div>
    <div id="CalenderModalAnular" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <form id="Anulaform" class="form-horizontal calender" role="form">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">ANULAR RESERVA</h4>
                    </div>
                    <div class="modal-body">
                        <div id="testmodal" style="padding: 5px 20px;">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label class="control-label">MOTIVO DE ANULACIÓN</label>
                                    <input type="hidden" id="idreserva2" name="idreserva2">
                                    <select name="motivoanula" id="motivoanula" class="form-control selectpicker" required="campo requerido"></select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label class="control-label">COMENTARIO</label>
                                    <textarea class="form-control" style="height:55px; text-transform: uppercase;" id="comentadmin" name="comentadmin" required=""></textarea>
                                    <input type="hidden" id="comentuser" name="comentuser" value="">
                                </div>
                            </div>    
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" id="enviar2" class="btn btn-primary">Guardar</button>
                    </div>

                </div>
            </form>
        </div>
    </div>

    <div id="CalenderModalCopy" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <form id="copiaform" class="form-horizontal calender" role="form">
                    <div class="modal-content">

                        <div class="modal-header">
                            <button type="button" class="close" id="cierracopia" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" id="myModalLabel">SOLICITAR RESERVA</h4>
                        </div>
                        
                        <div class="modal-body">
                            <div id="testmodal" style="padding: 5px 20px;">
                                <input type="hidden" id="idservicio_copia" name="idservicio_copia" value="">
                                <div id="contenido"></div>
                                <hr>
                                <div class="form-group">
                                    <div class="col-md-6" id="div_fecha_desde">
                                        <label class="control-label">FECHA DESDE</label>
                                        <input type="date" class="form-control selectpicker" id="fechadesde_copia" name="fechadesde_copia" required="">
                                    </div>
                                    <div id="div_desde_copia" class="col-md-6">
                                        <label class="control-label">HORA DESDE</label>
                                        <div class='input-group date' id='desdecopy'>
                                            <input type='text' name="desde_copia" id="desde_copia" class="form-control" required=""/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>

                                </div>    

                                <div class="form-group">    

                                    <div class="col-md-6" id="div_fecha_hasta">
                                        <label class="control-label">FECHA HASTA</label>
                                        <input type="date" class="form-control selectpicker" id="fechahasta_copia" name="fechahasta_copia" required="">
                                    </div>

                                    <div id="div_hasta_copia" class="col-md-6">
                                        <label class="control-label">HORA HASTA</label>
                                        <div class='input-group date' id='hastacopy'>
                                            <input type='text' name="hasta_copia" id="hasta_copia" class="form-control" required=""/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>                                
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <div class="col-sm-9">
                                        <label class="control-label">ADJUNTAR ARCHIVO (excel)</label>
                                        <input type="file" id="excel1" name="excel1" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" class="custom-file-input">
                                    </div>
                                    <div class="col-sm-3">
                                        <label class="control-label">FORMATO</label><br>
                                        <a href="../files/formatos/formato.xlsx" target="_blank"><img src="../files/icono/Excel-icon.png" height="30px" width="30px" ></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="submit" id="enviar_copia" class="btn btn-primary">Guardar</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>

    <div id="fc_create" data-toggle="modal" data-target="#CalenderModalNew"></div>
    <div id="fc_anular" data-toggle="modal" data-target="#CalenderModalAnular"></div>
    <div id="fc_copiar" data-toggle="modal" data-target="#CalenderModalCopy"></div>
    <?php
    require 'footer.php';
    ?>
    <script type="text/javascript" src="scripts/reserva.js"></script>


    <?php
}
ob_end_flush();


