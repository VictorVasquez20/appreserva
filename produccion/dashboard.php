<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>APP Reservas</title>

        <link rel="icon" href="../public/favicon.ico" type="image/x-icon" />

        <!-- Bootstrap -->
        <link href="../public/build/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="../public/build/css/font-awesome.min.css" rel="stylesheet">
        <!-- NProgress -->
        <link href="../public/build/css/nprogress.css" rel="stylesheet">
        <!-- iCheck -->
        <link href="../public/build/css/green.css" rel="stylesheet">

        <!-- Datatables -->
        <link href="../public/build/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="../public/build/css/buttons.bootstrap.min.css" rel="stylesheet">
        <link href="../public/build/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
        <link href="../public/build/css/responsive.bootstrap.min.css" rel="stylesheet">
        <link href="../public/build/css/scroller.bootstrap.min.css" rel="stylesheet">

        <!-- FORMS -->
        <link href="../public/build/css/prettify.min.css" rel="stylesheet">
        <link href="../public/build/css/select2.min.css" rel="stylesheet">
        <link href="../public/build/css/switchery.min.css" rel="stylesheet">
        <link href="../public/build/css/starrr.css" rel="stylesheet">

        <!-- bootstrap-file-imput -->
        <link href="../public/build/css/fileinput.min.css" rel="stylesheet">

        <!-- PNotify -->
        <link href="../public/build/css/pnotify.css" rel="stylesheet">
        <link href="../public/build/css/pnotify.buttons.css" rel="stylesheet">
        <link href="../public/build/css/pnotify.nonblock.css" rel="stylesheet">

        <!-- bootstrap-select -->
        <link href="../public/build/css/bootstrap-select.min.css" rel="stylesheet">

        <!-- bootstrap-daterangepicker -->
        <link href="../public/build/css/daterangepicker.css" rel="stylesheet">
        <!-- bootstrap-datetimepicker -->
        <link href="../public/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
        <!-- Ion.RangeSlider -->
        <link href="../public/build/css/normalize.css" rel="stylesheet">
        <link href="../public/build/css/ion.rangeSlider.css" rel="stylesheet">
        <link href="../public/build/css/ion.rangeSlider.skinFlat.css" rel="stylesheet">
        <!-- Bootstrap Colorpicker -->
        <link href="../public/build/css/bootstrap-colorpicker.min.css" rel="stylesheet">

        <!-- bootstrap-progressbar -->
        <link href="../public/build/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
        <!-- JQVMap -->
        <link href="../public/build/css/jqvmap.min.css" rel="stylesheet"/>
        <!-- bootstrap-daterangepicker -->
        <link href="../public/build/css/daterangepicker.css" rel="stylesheet">

        <!-- FullCalendar -->
        <link href="../public/build/css/fullcalendar.min.css" rel="stylesheet">
        <link href="../public/build/css/fullcalendar.print.css" rel="stylesheet" media="print">

        <!-- Multi select-->
        <link href="../public/build/css/multi-select/multi-select.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="../public/build/css/custom.css" rel="stylesheet">
    </head>

    <body class="nav-md" style="overflow-x: hidden; overflow-y: hidden;">
        <div class="right_col" role="main">
            <div class="row">
                <div class="col-md-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <div id='calendario'></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- jQuery -->
        <script src="../public/build/js/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="../public//build/js/bootstrap.min.js"></script>
        <!-- FastClick -->
        <script src="../public/build/js/fastclick.js"></script>
        <!-- NProgress -->
        <script src="../public/build/js/nprogress.js"></script>
        <!-- Chart.js -->
        <script src="../public/build/js/Chart.min.js"></script>
        <!-- gauge.js -->
        <script src="../public/build/js/gauge.min.js"></script>
        <!-- bootstrap-progressbar -->
        <script src="../public/build/js/bootstrap-progressbar.min.js"></script>
        <!-- iCheck -->
        <script src="../public/build/js/icheck.min.js"></script>

        <!-- bootstrap-daterangepicker -->
        <script src="../public/build/js/moment.min.js"></script>
        <script src="../public/build/js/daterangepicker.js"></script>

        <!-- FullCalendar -->
        <script src="../public/build/js/es.js"></script>
        <script src="../public/build/js/fullcalendar.min.js"></script>


        <!-- bootstrap-fileinput -->
        <script src="../public/build/js/fileinput.min.js"></script>
        <!-- bootstrap-select -->
        <script src="../public/build/js/bootstrap-select.min.js"></script>

        <!-- bootstrap-datetimepicker -->    
        <script src="../public/build/js/bootstrap-datetimepicker.min.js"></script>
        <!-- Ion.RangeSlider -->
        <script src="../public/build/js/ion.rangeSlider.min.js"></script>
        <!-- Bootstrap Colorpicker -->
        <script src="../public/build/js/bootstrap-colorpicker.min.js"></script>

        <!-- Datatables -->
        <script src="../public/build/js/jquery.dataTables.min.js"></script>
        <script src="../public/build/js/dataTables.bootstrap.min.js"></script>
        <script src="../public/build/js/dataTables.buttons.min.js"></script>
        <script src="../public/build/js/buttons.bootstrap.min.js"></script>
        <script src="../public/build/js/buttons.flash.min.js"></script>
        <script src="../public/build/js/buttons.html5.min.js"></script>
        <script src="../public/build/js/buttons.print.min.js"></script>
        <script src="../public/build/js/dataTables.fixedHeader.min.js"></script>
        <script src="../public/build/js/dataTables.keyTable.min.js"></script>
        <script src="../public/build/js/dataTables.responsive.min.js"></script>
        <script src="../public/build/js/responsive.bootstrap.js"></script>
        <script src="../public/build/js/dataTables.scroller.min.js"></script>
        <script src="../public/build/js/jszip.min.js"></script>
        <script src="../public/build/js/pdfmake.min.js"></script>
        <script src="../public/build/js/vfs_fonts.js"></script>

        <!-- Bootbox Alert -->
        <script src="../public/build/js/bootbox.min.js"></script>
        <!-- jQuery Tags Input -->
        <script src="../public/build/js/jquery.tagsinput.js"></script>

        <!-- jquery.inputmask -->
        <script src="../public/build/js/jquery.inputmask.bundle.min.js"></script>

        <!-- bootstrap-wysiwyg -->
        <script src="../public/build/js/bootstrap-wysiwyg.min.js"></script>
        <script src="../public/build/js/jquery.hotkeys.js"></script>
        <script src="../public/build/js/prettify.js"></script>

        <!-- PNotify -->
        <script src="../public/build/js/pnotify.js"></script>
        <script src="../public/build/js/pnotify.buttons.js"></script>
        <script src="../public/build/js/pnotify.nonblock.js"></script>

        <!-- Switchery -->
        <script src="../public/build/js/switchery.min.js"></script>
        <!-- Select2 -->
        <script src="../public/build/js/select2.full.min.js"></script>

        <!-- Autosize -->
        <script src="../public/build/js/autosize.min.js"></script>
        <!-- jQuery autocomplete -->
        <script src="../public/build/js/jquery.autocomplete.min.js"></script>
        <!-- starrr -->
        <script src="../public/build/js/starrr.js"></script>

        <!-- morris.js -->
        <script src="../public/build/js/raphael.min.js"></script>
        <script src="../public/build/js/morris.min.js"></script>

        <!-- Flot -->
        <script src="../public/build/js/jquery.flot.js"></script>
        <script src="../public/build/js/jquery.flot.pie.js"></script>
        <script src="../public/build/js/jquery.flot.time.js"></script>
        <script src="../public/build/js/jquery.flot.stack.js"></script>
        <script src="../public/build/js/jquery.flot.resize.js"></script>
        <!-- Flot plugins -->
        <script src="../public/build/js/jquery.flot.orderBars.js"></script>
        <script src="../public/build/js/jquery.flot.spline.min.js"></script>
        <script src="../public/build/js/curvedLines.js"></script>
        <!-- DateJS -->
        <script src="../public/build/js/date.js"></script>
        <!-- JQVMap -->
        <script src="../public/build/js/jquery.vmap.js"></script>
        <script src="../public/build/js/jquery.vmap.world.js"></script>
        <script src="../public/build/js/jquery.vmap.sampledata.js"></script>


        <script type="text/javascript" src="../public/build/js/echarts.min.js"></script>

        <script src="../public/build/js/multi-select/jquery.multi-select.js"></script>

        <!-- Custom Theme Scripts -->
        <script src="../public/build/js/custom.js"></script>

        <script type="text/javascript" src="scripts/notificaciones.js"></script>
        <script type="text/javascript" src="scripts/dashboard.js"></script>
        
    </body>
</html>