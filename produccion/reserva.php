<?php
ob_start();
session_start();

if (!isset($_SESSION["nombre"])) {
    header("Location:login.php");
} else {

    require 'header.php';

    if ($_SESSION['administrador'] == 1) {
        ?>

        <div class="right_col" role="main">
            <div class="row">
                <div class="col-md-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Calendario de Reservas</h2>
                            <div id="filtros" style="float: right" class="dt-buttons btn-group">
                                <p class="btn">FILTROS</p>
                                <select id="filtros" class="form-control" onchange="calendar();">
                                    <option value="0,1,2,3">Todos</option>
                                    <option value="1">Agendados</option>
                                    <option value="2">Rechazados</option>
                                    <option value="3">Anulados</option>
                                </select>
                                <!-- <a class="btn btn-default buttons-copy buttons-html5" onclick="listarEstado('1,2,3', 0);">Todos</a>
                                <a class="btn btn-default buttons-copy buttons-html5" style="color:#060;" onclick="listarEstado(1, 0);">Agendados</a>
                                <a class="btn btn-default buttons-copy buttons-html5" style="color:#E74C3C;" onclick="listarEstado(2, 0);">Rechazados</a> 
                                <a class="btn btn-default buttons-copy buttons-html5" style="color: #269abc;" onclick="listarEstado(3, 0);">Anulados</a> --> 
                                <!--<a class="btn btn-default buttons-copy buttons-html5" onclick="Listartodos();">Todos</a>
                                <a class="btn btn-default buttons-copy buttons-html5" style="color:#060;" onclick="listar();">Agendados</a>
                                <a class="btn btn-default buttons-copy buttons-html5" style="color:#E74C3C;" onclick="rechazados();">Rechazados</a> -->
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div id='calendario'></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="CalenderModalNew" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">NUEVA RESERVA</h4>
                    </div>
                    <form id="antoform" class="form-horizontal calender" role="form">
                        <div class="modal-body">
                            <div id="testmodal" style="padding: 5px 20px;">
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <label class="control-label">ITEM</label>
                                        <select name="item" id="item" class="form-control selectpicker" required="" data-live-search="true"></select>
                                        <input type="hidden" id="fechadesde" name="fechadesde">
                                        <input type="hidden" id="fechahasta" name="fechahasta">
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="control-label">TIPO</label>
                                        <select name="treserva" id="treserva" class="form-control selectpicker" required="">
                                            <option value="0">DESPACHO</option>
                                            <option value="1">RETIRO</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label class="control-label">RESPONSABLE</label>
                                        <select name="responsable" id="responsable" class="form-control selectpicker" required="" data-live-search="true"></select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label class="control-label">CENTRO COSTO</label>
                                        <select name="centrocosto" id="centrocosto" class="form-control selectpicker" required="" data-live-search="true"></select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <label class="control-label">DESDE</label>
                                        <p id="fechadesde3"></p>
                                        <div class='input-group date' id='myDatepicker_desde'>
                                            <input type='text' name="desde" id="desde" class="form-control" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="control-label">HASTA</label>
                                        <p id="fechahasta3"></p>
                                        <div class='input-group date' id='myDatepicker_hasta'>
                                            <input type='text' name="hasta" id="hasta" class="form-control" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">    
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="control-label">DIRECCIÓN</label>
                                            <input name="direccion" id="direccion" class="form-control selectpicker" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-8">
                                        <label class="control-label">CONTACTO EN OBRA (Fabrimetal)</label>
                                        <input name="recibe" id="recibe" style="text-transform: uppercase;" class="form-control selectpicker" required="">
                                    </div>

                                    <div class="col-sm-4">
                                        <label class="control-label">TELEFONO</label>
                                        <input name="telefono" id="telefono" class="form-control selectpicker" required="">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="col-sm-8">
                                        <label class="control-label">CONTACTO DE OBRA</label>
                                        <input name="contactoobra" id="contactoobra"  style="text-transform: uppercase;" class="form-control selectpicker" required="">
                                    </div>

                                    <div class="col-sm-4">
                                        <label class="control-label">TELEFONO</label>
                                        <input name="fonoobra" id="fonoobra" class="form-control selectpicker" required="">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label class="control-label">DESCRIPCIÓN</label>
                                        <textarea class="form-control" style="height:55px; text-transform: uppercase;" id="descr" name="descr"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label class="control-label">SOLICITADO POR</label>
                                        <select id="solicita" name="solicita" class="form-control"  data-live-search="true" ></select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-9">
                                        <label class="control-label">ADJUNTAR ARCHIVO (excel)</label>
                                        <input type="file" id="excel" name="excel" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" class="custom-file-input">
                                    </div>
                                    <div class="col-sm-3">
                                        <label class="control-label">FORMATO</label><br>
                                        <a href="../files/formatos/formato.xlsx" target="_blank"><img src="../files/icono/Excel-icon.png" height="30px" width="30px" ></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default antoclose" data-dismiss="modal">Cerrar</button>
                            <button type="submit" id="enviar" class="btn btn-primary" >Guardar</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>


        <div id="CalenderModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myModalLabel">RESERVA</h4>
                         <span id="created_time" style="color:#EE6709;"></span>
                    </div>

                    <form id="reservaform" class="form-horizontal calender" role="form">
                        <div class="modal-body">
                            <div id="testmodal" style="padding: 5px 20px;">
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <label class="control-label">ITEM</label>
                                        <select name="item2" id="item2" class="form-control selectpicker" required=""></select>
                                        <input type="hidden" id="fechadesde2" name="fechadesde2">
                                        <input type="hidden" id="fechahasta2" name="fechahasta2">
                                        <input type="hidden" id="reserva2" name="reserva2">
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="control-label">TIPO</label>
                                        <select name="treserva1" id="treserva1" class="form-control selectpicker" required="" disabled="">
                                            <option value="0">DESPACHO</option>
                                            <option value="1">RETIRO</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label class="control-label">RESPONSABLE</label>
                                        <select name="responsable2" id="responsable2" class="form-control selectpicker" required=""></select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label class="control-label">CENTRO COSTO</label>
                                        <select name="centrocosto2" id="centrocosto2" class="form-control selectpicker" disabled=""></select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <label class="control-label">DESDE</label>
                                        <p id="fechadesde4"></p>
                                        <div class='input-group date' id='myDatepicker'>
                                            <input type='text' name="desde2" id="desde2" class="form-control" readonly="true"/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="control-label">HASTA</label>
                                        <p id="fechahasta4"></p>
                                        <div class='input-group date' id='myDatepicker3'>
                                            <input type='text' name="hasta2" id="hasta2" class="form-control" readonly="true" />
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group product_price">
                                    <div class="col-sm-12">
                                        <label class="control-label">DESCRIPCIÓN: </label>
                                        <input type="hidden" id="descr2" name="descr2">
                                        <span id="descr3"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label class="control-label">SOLICITANTE</label>
                                        <select id="solicita2" name="solicita2" class="form-control" disabled=""></select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-9">
                                        <label class="control-label">ARCHIVO ADJUNTO</label><br>
                                        <span id="adjunto"></span>
                                    </div>    
                                </div>
                                <hr>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label class="control-label">COMENTARIO ADMINISTRADOR</label>
                                        <textarea class="form-control" style="height:55px; text-transform: uppercase;" id="coment2" name="coment2"></textarea>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default antoclose" data-dismiss="modal">Cerrar</button>
                            <button type="button" id="enviar" class="btn btn-danger antosubmit" onclick="eliminar();">Rechazar</button>
                            <button type="submit" id="enviar" class="btn btn-primary">Agendar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div id="CalenderModalAnular" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <form id="Anulaform" class="form-horizontal calender" role="form">
                    <div class="modal-content">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" id="myModalLabel">ANULAR RESERVA</h4>
                        </div>
                        <div class="modal-body">
                            <div id="testmodal" style="padding: 5px 20px;">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label class="control-label">Motivo de Anulación</label>
                                        <input type="hidden" id="idreserva2" name="idreserva2">
                                        <select name="motivoanula" id="motivoanula" class="form-control selectpicker" required="campo requerido"></select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <label class="control-label">COMENTARIO</label>
                                        <textarea class="form-control" style="height:55px; text-transform: uppercase;" id="comentadmin" name="comentadmin" required=""></textarea>
                                        <input type="hidden" id="comentuser" name="comentuser" value="">
                                    </div>
                                </div>    
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="submit" id="enviar" class="btn btn-primary">Guardar</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>

        

        <div id="fc_anular" data-toggle="modal" data-target="#CalenderModalAnular"></div>
        <div id="fc_create" data-toggle="modal" data-target="#CalenderModalNew"></div>
        <div id="fc_edit" data-toggle="modal" data-target="#CalenderModalEdit"></div>
        


        <?php
    } else {
        require 'nopermiso.php';
    }
    require 'footer.php';
    ?>
    <script type="text/javascript" src="scripts/reserva.js"></script>
    <?php
}
ob_end_flush();

