<?php
ob_start();
session_start();

if(!isset($_SESSION["nombre"])){
  header("Location:login.php");
}else{

require 'header.php';

?>

<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Items para reserva</h2>
                        <div class="clearfix"></div>

                        <button id="op-agregar" name="op-agregar" onclick="mostrarform(true)" class="btn btn-primary">Agregar</button>
                        <!--<button id="op-listar" name="op-listar" style="display:none;" onclick="mostrarform(true)" class="btn btn-primary">Listado</button>-->
                    </div>

                    <div class="x_content">

                        <div id="tablaitem">
                            <table id="tblitem" name="tblcategoria" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead>
                                  <tr>
                                    <th></th>
                                    <th>ID</th>
                                    <th>Nombre</th>
                                    <th>Color</th>
                                    <th>Vigente</th>
                                  </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>

                        <div id="formitem" style="display: none;">
                            <form id="frmpitem" class="form-horizontal form-label-left input_mask">
                                <div class="form-group">
                                    <!--<label class="control-label col-md-2 col-sm-2 col-xs-12">id interno sistema</label>-->
                                    <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                                        <!--<input type="hidden" class="form-control has-feedback-left" id="idempleado" name="idempleado">-->
                                        <input type="hidden" class="form-control has-feedback-left" id="iditem" name="iditem" readonly="">
                                       <!--<span class="fa fa-id-card form-control-feedback left" aria-hidden="true"></span>-->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-12">Nombre <span class="required">*</span></label>
                                    <div class="col-md-8 col-sm-8 col-xs-12 form-group has-feedback">
                                        <input type="text" style="text-transform:uppercase;" class="form-control has-feedback-left" required="required" id="nombre" name="nombre" placeholder="Nombre">
                                        <span class="fa fa-archive form-control-feedback left" aria-hidden="true"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-12">color <span class="required">*</span></label>
                                    <div class="col-md-8 col-sm-8 col-xs-12">
                                      <div class="input-group colores">
                                          <input type="text" id="color" name="color" class="form-control" />
                                        <span class="input-group-addon"><i></i></span>
                                      </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-12">Vigencia</label>
                                    <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                                        <div class="">
                                            <label>
                                                <input name="vigencia" id="vigencia" type="checkbox" class="icheckbox_flat-green" value="1"/>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <button id="op-cancelar" name="op-cancelar" onclick="mostrarform(false)" class="btn btn-primary">Volver</button>
                                <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiar()">Limpiar</button>
                                <button class="btn btn-success" type="submit" id="btnGuardar">Guardar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php
require 'footer.php';
?>
<script type="text/javascript" src="scripts/item.js"></script> 
<?php
}
ob_end_flush();

