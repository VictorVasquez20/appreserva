var tabla;

//funcion que se ejecuta iniciando
function init() {
    mostrarform(false);
    listar();
    $("#formulario").on("submit", function (e) {
        e.preventDefault();
        guardaryeditar();
    });
}


// Otras funciones
function limpiar() {
    $("#idresponsable").val("");
    $("#nombre").val("");
    $("#emailEmpresa").val("");
    
   $("#condicion").attr("checked",false);
}

function mostrarform(flag) {
    limpiar();
    if (flag) {
        $("#listadoresponsable").hide();
        $("#formularioresponsable").show();
        $("#op_agregar").hide();
        $("#op_listar").show();
        $("#btnGuardar").prop("disabled", false);

    } else {
        $("#listadoresponsable").show();
        $("#formularioresponsable").hide();
        $("#op_agregar").show();
        $("#op_listar").hide();
    }

}

function cancelarform() {
    limpiar();
    mostrarform(false);
}

function listar() {
    tabla = $('#tblresponsable').dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Bfrtip',
        buttons: Botones,
        "language": Español,
        "ajax": {
            url: '../ajax/responsable.php?op=listar',
            type: "get",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 10, //Paginacion 10 items
        "order": [[1, "desc"]] //Ordenar en base a la columna 0 descendente
    }).DataTable();
}

function guardaryeditar() {
    $("#btnGuardar").prop("disabled", true);
    var formData = new FormData($("#formulario")[0]);
    $.ajax({
        url: '../ajax/responsable.php?op=guardaryeditar',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,

        success: function (datos) {
            //bootbox.alert(datos);
            if (datos != '0') {
                $("#btnGuardar").prop("disabled", false);
                new PNotify({
                    title: 'Correcto!',
                    text: 'Responsable guardado con exito.',
                    type: 'success',
                    styling: 'bootstrap3'
                });
            } else {
                $("#btnGuardar").prop("disabled", true);
                new PNotify({
                    title: 'Error!',
                    text: 'Responsable no guardado.',
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
            mostrarform(false);
            tabla.ajax.reload();
        }
    });
    limpiar();
}

function mostrar(idresponsable){
    $.post("../ajax/responsable.php?op=mostrar",{"idresponsable" : idresponsable}, function(data){
        data = JSON.parse(data);
        mostrarform(true);

        $("#idresponsable").val(data.idresponsable);
        $("#nombre").val(data.nombre);
        $("#emailEmpresa").val(data.emailEmpresa);
        
        if(data.condicion == 1){
            $("#condicion").attr("checked",true);
            $('.js-switch').trigger('click');
        }else{
            $("#condicion").attr("checked",false);
        }		
    });
}

init();

