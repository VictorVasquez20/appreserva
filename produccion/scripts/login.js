/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 
*/

/**
 * cuando el focus sale del input verifico si el rut es correcto
 * @type type
 */
$("#rut").blur(function(){
    if(this.value != ""){
        if(!validar(this.value)){
            bootbox.alert("Rut incorrecto");
        }
    }
});

/**
 * Valida si rut es correcto
 * @param {string} rut
 * @returns {Boolean}
 */
function validar(rut){
    if (!/[0-9]{1,2}.[0-9]{3}.[0-9]{3}-[0-9Kk]{1}/.test(rut))
    return false;
    var tmp = rut.split('-');
    var dv1 = tmp[1], rut = tmp[0].split('.').join('');
    if(dv1 == 'K') dv1 = 'k';
    return (dv(rut) == dv1);
}
/**
 * calcula el digito verificador 
 * @param {string} rut
 * @returns {Number|String}
 */
function dv(rut){
    var M=0,S=1;
    for(;rut;rut=Math.floor(rut/10))
    S=(S+rut%10*(9-M++%6))%11;
    return S ? S-1 : 'k';
}

                   
/**
 * comprueba si el trbajador existe y lo deja entrar al sistema
 */
$("#frmAcceso").on('submit', function(e){
    e.preventDefault();
    rut = $("#rut").val();
    if(validar(rut)){
        console.log(rut);
        $.post("../ajax/empleadoAjax.php?op=selectrut",{"rut": rut}, function(data){
            if(data != "null"){
                $(location).attr("href", "calendario.php");
            }else{
                bootbox.alert("No existe como trabajador, favor ponerse en contacto con Recursos humanos de la empresa.");
            }
        });
    }else{
        bootbox.alert("Rut incorrecto.");
    }
});

/**
 * Comprueba si el usuario es correcto y lo deja entrar al sistema
 */
//console.log("Cargo");
$("#frmAccesoadmin").on('submit', function(e){
	e.preventDefault();
	var username = $("#username").val();
	var password = $("#password").val();
	//console.log(username);
        //alert(username);
	$.post("../ajax/usuarioAjax.php?op=verificar",{"username_form": username, "password_form": password}, function(data){
		if(data!="null"){
			$(location).attr("href", "reserva.php");
		}else{
			bootbox.alert("Usuario o Password Incorrectos");
		}

	});

});