/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function init(){
    listar();
    //$('.colores').colorpicker();
    
    $("#frmpitem").on("submit", function(e){
        guardaryeditar(e);
    });
}

function limpiar(){
    $("#iditem").val("");
    $("#nombre").val("");
    $("#color").val("");
}

function listar(){
tabla=$('#tblitem').dataTable({
        "aProcessing":true,
        "aServerSide": true,
        dom: 'Bfrtip',
        buttons:Botones,
        "language":Español,
        "ajax":{
                url:'../ajax/itemAjax.php?op=listar',
                type:"get",
                dataType:"json",
                error: function(e){
                        console.log(e.responseText);
                }
        },
        "bDestroy": true,
        "iDisplayLength": 10, //Paginacion 10 items
        "order" : [[1 , "desc"]] //Ordenar en base a la columna 0 descendente
}).DataTable();
}


function mostrarform(flag){
    limpiar();
    if(flag){
        $("#tablaitem").hide();
        $("#formitem").show();
        $("#op_agregar").hide();
        $("#op_listar").show();
        $("#btnGuardar").prop("disabled", false);
    }else{
        $("#tablaitem").show();
        $("#formitem").hide();
        $("#op_agregar").show();
        $("#op_listar").hide();
    }
}

function cancelarform(){
    limpiar();
    mostrarform(false);
}

function guardaryeditar(e){
	e.preventDefault();
	$("#btnGuardar").prop("disabled", true);
	var formData = new FormData($("#frmpitem")[0]);
	$.ajax({
		url:'../ajax/itemAjax.php?op=guardaryeditar',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
			bootbox.alert(datos);
			mostrarform(false);
			tabla.ajax.reload();
		}
	});
	limpiar();
}


function mostrar(iditem){
    $.post("../ajax/itemAjax.php?op=mostrar",{ iditem: iditem }, function(data){
            data = JSON.parse(data);
            mostrarform(true);

            $("#iditem").val(data.iditem);
            $("#nombre").val(data.nombre);
            $("#color").val(data.color);
            $(".colores").colorpicker();
            $("#colores").trigger('keypress', $(".colores").colorpicker('setValue', data.color));
            
            if(data.vigencia){
                $("#vigencia").attr("checked",true);
            }else{
                $("#vigencia").attr("checked",false);
            }


    });
}
init();