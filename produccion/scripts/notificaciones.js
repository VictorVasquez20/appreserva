/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function init(){
    //notificacion();
}

function notificacion(){
    var admin = $("#notificacionAdmin").val();
    $.post("../ajax/reservaAjax.php?op=notificacion", function(data){
        $("#menu1").empty();
        if(data != null && data.length > 2){
            data = JSON.parse(data);
            if (admin == 1){
                for(i = 0; i < data.length; i++){

                    $("#notificacionNumero").append('<span class="badge bg-info" style="background-color: red;">'+ data.length +'</span>');

                    $("#menu1").append(
                            '<li>'
                        +   '  <a href="#" onclick="mostrar(' + data[i]['idreserva']  + ');return false;" >'
                        +   '      <span>'
                        +   '          <span><b>'+ data[i]['nombitem'] +'</b></span> <br>'
                        +   '          <span>'+ data[i]['nomb']+'</span> <br>'
                        +   '          <span>'+ formatofecha2(data[i]['desde'])+ ' hasta ' + formatofecha2(data[i]['hasta']) +'</span>'
                        +   '      </span><br>'
                        +   '      <span class="message">'
                        +   '          "'+data[i]['descripcion'].substr(0,30) + '..."'
                        +   '      </span>'
                        +   '   </a>'
                        +   '</li>'
                       );
               }    
           }else{
                $("#notificacionNumero").empty();
                $("#notificacionNumero").append('<i class="fa fa-envelope-o"></i>');
                $("#menu1").append('<li> <a href="#"> <span> <span>No hay notificaciones nuevas</b></span> <br> </span><br> </a> </li>');
           }
        }else{
            $("#notificacionNumero").empty();
            $("#notificacionNumero").append('<i class="fa fa-envelope-o"></i>');
            $("#menu1").append('<li> <a href="#"> <span> <span>No hay notificaciones nuevas</b></span> <br> </span><br> </a> </li>');
        }
    });
    
}
/**
 * le da formato a una fecha
 * @param {string} texto
 * @returns {String}
 */
function formatofecha2(texto){
    if(texto != ""){
        if(texto != null){
            var fecha = texto.split(" ");
            var info = fecha[0].split('-');
            return info[2] + '/' + info[1] + '/' + info[0] + ' a las ' +fecha[1];
        }
    }
}

/**
 * le da formato a una fecha
 * @param {string} texto
 * @returns {String}
 */
function formatofechacorta(texto) {
    if (texto != "") {
        if (texto != null) {
            var fecha = texto.split(" ");
            var info = fecha[0].split('-');
            return info[2] + '/' + info[1] + '/' + info[0];
        }
    }
}


init();