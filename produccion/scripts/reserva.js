/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* CALENDAR */

function init() {
    calendar();
    calendario();

    //listar();
    //listaruser();

    $('#myDatepicker_desde').datetimepicker({
        format: 'HH:mm',
        ignoreReadonly: true
    });

    $('#myDatepicker_hasta').datetimepicker({
        format: 'HH:mm',
        ignoreReadonly: true
    });

    $('#desdecopy').datetimepicker({
        format: 'HH:mm',
        ignoreReadonly: true
    });

    $('#hastacopy').datetimepicker({
        format: 'HH:mm',
        ignoreReadonly: true
    });

    $("#myDatepicker_desde").on("dp.hide", function (e) { //evento cuando se cierra el calendario.
        if ($('#hasta').val() != "") {
            validar_fechas();
        }
    });

    $("#myDatepicker_hasta").on("dp.hide", function (e) { //evento cuando se cierra el calendario.
        if ($('#desde').val() != "") {
            validar_fechas();
        }
    });

    $("#desdecopy").on("dp.hide", function (e) { //evento cuando se cierra el calendario.
        if ($('#hasta_copia').val() != "") {
            validar_fechas();
        }
    });

    $("#hastacopy").on("dp.hide", function (e) { //evento cuando se cierra el calendario.
        if ($('#desde_copia').val() != "") {
            validar_fechas();
        }
    });
    
    
    $("#fechadesde_copia").on("focusout", function (e) { //evento cuando se cierra el calendario.
        if ($('#fechahasta_copia').val() != "") {
            validar_fechas();
        }
    });

    $("#fechahasta_copia").on("focusout", function (e) { //evento cuando se cierra el calendario.
        if ($('#fechadesde_copia').val() != "") {
            validar_fechas();
        }
    });

    $.post("../ajax/itemAjax.php?op=selectitem", function (r) {
        $("#item").html(r);
        $("#item").selectpicker('refresh');
    });

    $.post("../ajax/responsable.php?op=selectresponsable", function (r) {
        $("#responsable").html(r);
        $("#responsable").selectpicker('refresh');

        $("#responsable2").html(r);
        $("#responsable2").selectpicker('refresh');
    });

    $.post("../ajax/motivoanulacion.php?op=selectmotivoanulacion", function (r) {
        $("#motivoanula").html(r);
        $("#motivoanula").selectpicker('refresh');
    });


    $.post("https://www.fabrimetalsa.cl/appfabrimetal/ajax/ApiExterna.php?apicall=SelectCentrosCosto", function (x) {
        data = JSON.parse(x);
        //console.log(data);
        $("#centrocosto").html(data.contenido);
        $("#centrocosto").selectpicker('refresh');
    });

    $.post("../ajax/empleadoAjax.php?op=selectempleado", function (r) {
        $("#solicita").html(r);
        $("#solicita").selectpicker('refresh');
    });

    $("#reservaform").on("submit", function (e) {
        e.preventDefault();
        editar();
    });

    $("#antoform").on("submit", function (e) {
        e.preventDefault();
        agregar();
    });

    $("#Solicitaform").on("submit", function (e) {
        e.preventDefault();
        solicitar();
    });

    $("#Anulaform").on("submit", function (e) {
        e.preventDefault();
        anular();
    });

    $("#copiaform").on("submit", function (e) {
        e.preventDefault();
        solicitaCopia();
    });

    $('#CalenderModalNew').on('shown.bs.modal', function () {
        $("#telefono").inputmask("+56(9)9999-9999"); //default        
        $("#fonoobra").inputmask("+56(9)9999-9999")
    });

    $('#CalenderModalNew').on('hidden.bs.modal', function (e) {
        $("#enviar").prop("disabled", false);
        $("#desde").val("");
        $("#hasta").val("");
        $("#div_hasta").removeClass("has-error");
        $("#div_desde").removeClass("has-error");
    });

}

$("#telefono").focusout(function () {
    var valor = $("#telefono").val();

    if (valor.length <= 15) {
        if (valor.includes('_')) {
            new PNotify({
                title: 'Error!',
                text: 'El Telefono debe tener 9 caracteres"',
                type: 'error',
                styling: 'bootstrap3'
            });
            $("#telefono").focus();
        }
    }
});


$("#fonoobra").focusout(function () {
    var valor = $("#fonoobra").val();

    if (valor.length <= 15) {
        if (valor.includes('_')) {
            new PNotify({
                title: 'Error!',
                text: 'El Telefono debe tener 9 caracteres"',
                type: 'error',
                styling: 'bootstrap3'
            });
            $("#fonoobra").focus();
        }
    }
});

function calendar() {

    if (typeof ($.fn.fullCalendar) === 'undefined') {
        return;
    }

    $('#calendario').fullCalendar('removeEvents', function () {
        return true;
    });
    var test = $("#filtros option").filter(":selected").val();
    var solicita = 0;
    console.log(test);
    console.log('calendar');
    $('#calendario').fullCalendar('refetchEvents');

    var date = new Date(),
            d = date.getDate(),
            m = date.getMonth(),
            y = date.getFullYear(),
            started,
            categoryClass;

    var calendar = $('#calendario').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay,listMonth'
        },
        locale: 'es',
        /* EDITAR */
        editable: true,
        selectable: true,
        /* EDITAR */
        eventLimit: false,
        selectHelper: false,
        //events: "../ajax/reservaAjax.php?op=lista",
        events: {
            url: '../ajax/reservaAjax.php',
            method: 'GET',
            data:function () { // a function that returns an object
                return {
                    op: 'lista',
                estado: $("#filtros option").filter(":selected").val(),
                estado2: $("#filtros option").filter(":selected").val(),
                solicita: solicita,
                start: $('#calendario').fullCalendar('getView').start,
                end: $('#calendario').fullCalendar('getView').end
                };
            },
            /*data: {
                op: 'lista',
                estado: $("#filtros option").filter(":selected").val(),
                estado2: $("#filtros option").filter(":selected").val(),
                solicita: solicita,
                start: $('#calendario').fullCalendar('getView').start,
                end: $('#calendario').fullCalendar('getView').end
            },*/
        },
        select: function (start, end, allDay) {
            var s = new Date(start);
            s.setDate(s.getDate() + 1);
            var e = new Date(end);
            var f = new Date();

            if (s.getTime() < f.getTime()) {
                new PNotify({
                    title: 'OPS!',
                    text: 'No puede reservar una fecha menor a hoy.',
                    type: 'info',
                    styling: 'bootstrap3'
                });
                return false;
                //bootbox.alert("No puede reservar una fecha menor a hoy");
                //return false;
            }

            $.post("../ajax/reservaAjax.php?op=sessionactiva", function (sw) {
                if (isNaN(sw)) {
                    window.location = "login.php";
                    return false;
                }
            });
            $('#fc_create').click();

            $("#fechadesde").val(s.getFullYear() + "-" + (s.getMonth() + 1) + "-" + (s.getDate()));
            $("#fechahasta").val(e.getFullYear() + "-" + (e.getMonth() + 1) + "-" + (e.getDate()));
            $("#fechadesde3").html((s.getDate()) + "-" + (s.getMonth() + 1) + "-" + s.getFullYear());
            $("#fechahasta3").html((e.getDate()) + "-" + (e.getMonth() + 1) + "-" + e.getFullYear());

            started = start;
            ended = end;
            $(".antosubmit").on("click", function () {
                //categoryClass = $("#event_type").val();
                calendar.fullCalendar('unselect');
                $('.antoclose').click();
                return false;

            });
        },
        eventClick: function (calEvent, jsEvent, view) {
            var s = new Date(calEvent.start);
            var e = new Date(calEvent.end);
            s = s.getUTCDate() + "-" + (s.getUTCMonth() + 1) + "-" + s.getUTCFullYear() + " a las " + s.getUTCHours() + ":" + s.getMinutes() + " Hrs.";
            e = e.getUTCDate() + "-" + (e.getUTCMonth() + 1) + "-" + e.getUTCFullYear() + " a las " + e.getUTCHours() + ":" + e.getMinutes() + " Hrs.";

            console.log(calEvent);

            var event = '<span style="color:#EE6709;">Fecha creación: ' + formatofecha(calEvent.created_time) + '</span>' + '<br><br>' +
                    '<b>RESERVA: </b>     ' + calEvent.title + ' -  <b> (PARA ' + (calEvent.treserva == '1' ? 'RETIRO' : 'DESPACHO') + ')</b> <br>' +
                    '<b>RESPONSABLE: </b>' + calEvent.nombresponsable + '<br>' +
                    '<b>CENTRO COSTO: </b>' + calEvent.centrocosto + '<br>' +
                    '<b>SOLICITADO POR: </b>' + calEvent.nomb + '<br>' +
                    '<b>INICIO: </b>      ' + s + '<br>' +
                    '<b>TERMINO: </b>     ' + e + '<br>' +
                    '<b>DESCRIPCIÓN: </b> ' + calEvent.description + '<br>' +
                    '<b>ADJUNTO: </b> ' + (calEvent.file ? '<a href="../files/transporte/' + calEvent.file + '" target="_blank"><img src="../files/icono/Excel-icon.png" height="30px" width="30px" ></a>' : "") + '<br> <hr>' +
                    '<b>COMENTARIO </b> ' + calEvent.comentario ;

            var Botones2 = {cancel: {label: 'Cerrar', className: 'btn-info'}};

            if (calEvent.estado == 1) {
                var d = new Date(calEvent.start);
                var f = new Date();
                if (d.getDate() < f.getDate()) {
                    Botones2 = {cancel: {label: 'Cerrar', className: 'btn-info'}};
                } else {
                    Botones2 = {cancel: {label: 'Cerrar', className: 'btn-info'},
                        /*ok: {label: "rechazar", className: 'btn-danger', callback: function () {
                         eliminar(calEvent.idreserva, true);
                         }},*/
                        otro: {label: "Anular", className: 'btn-dark', callback: function () {
                                $("#fc_anular").trigger('click');
                                $("#idreserva2").val(calEvent.idreserva);
                            }}
                    };
                }
            }

            bootbox.dialog({
                title: '<b>' + calEvent.title + '</b>',
                inputType: 'text',
                message: event,
                buttons: Botones2

            });

            if (calEvent.url) {
                window.open(calEvent.url);
                return false;
            }

            calendar.fullCalendar('unselect');
        }
    });
notificacion();

}

function calendario() {

    if (typeof ($.fn.fullCalendar) === 'undefined') {
        return;
    }
    $('#calendario2').fullCalendar('removeEvents', function () {
        return true;
    });

    var solicita = 1;
    console.log('calendario2');
    $('#calendario2').fullCalendar('refetchEvents');

    var date = new Date(),
            d = date.getDate(),
            m = date.getMonth(),
            y = date.getFullYear(),
            started,
            categoryClass;

    var calendar = $('#calendario2').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay,listMonth'
        },

        locale: 'es',
        /* EDITAR */
        editable: true,
        selectable: true,
        /* EDITAR */
        eventLimit: false,
        selectHelper: false,
        events: {
            url: '../ajax/reservaAjax.php',
            method: 'GET',
            data:function () { // a function that returns an object
                return {
                    op: 'lista',
                estado: $("#filtros option").filter(":selected").val(),
                estado2: $("#filtros option").filter(":selected").val(),
                solicita: solicita,
                start: $('#calendario2').fullCalendar('getView').start,
                end: $('#calendario2').fullCalendar('getView').end
                };
            },
        },
        select: function (start, end, allDay) {


            var s = new Date(start);
            s.setDate(s.getDate() + 1);
            var e = new Date(end);
            var f = new Date();

            var fd = s.getFullYear() + "-" + (s.getMonth() + 1) + "-" + (s.getDate());
            var fh = e.getFullYear() + "-" + (e.getMonth() + 1) + "-" + (e.getDate());


            if (s.getTime() < f.getTime()) {
                new PNotify({
                    title: 'OPS!',
                    text: 'No puede reservar una fecha menor a hoy.',
                    type: 'info',
                    styling: 'bootstrap3'
                });
                return false;
                //bootbox.alert("No puede reservar una fecha menor a hoy");
                //return false;
            }

            $.post("../ajax/reservaAjax.php?op=sessionactiva", function (sw) {
                if (isNaN(sw)) {
                    window.location = "login.php";
                    return false;
                }
            });

            $('#fc_create').click();

            $.post("../ajax/itemAjax.php?op=selectitemCERO", function (r) {
                $("#item").html(r);
                $("#item").selectpicker('refresh');
            });

            $("#fechadesde").val(fd);
            $("#fechahasta").val(fh);
            $("#fechadesde3").html((s.getDate()) + "-" + (s.getMonth() + 1) + "-" + s.getFullYear());
            $("#fechahasta3").html((e.getDate()) + "-" + (e.getMonth() + 1) + "-" + e.getFullYear());

            started = start;
            ended = end;
            $(".antosubmit").on("click", function () {
                //categoryClass = $("#event_type").val();
                calendar.fullCalendar('unselect');
                //$('.antoclose').click();
                return false;

            });
        },
        eventClick: function (calEvent, jsEvent, view) {
            var s = new Date(calEvent.start);
            var e = new Date(calEvent.end);
            s = s.getUTCDate() + "-" + (s.getUTCMonth() + 1) + "-" + s.getUTCFullYear() + " a las " + s.getUTCHours() + ":" + s.getMinutes() + " Hrs.";
            e = e.getUTCDate() + "-" + (e.getUTCMonth() + 1) + "-" + e.getUTCFullYear() + " a las " + e.getUTCHours() + ":" + e.getMinutes() + " Hrs.";


            var event = '<span style="color:#EE6709;">Fecha creación: ' + formatofecha(calEvent.created_time) + '</span>' + '<br><br>' +
                    '<b>RESERVA: </b>     ' + calEvent.title + '<br>' +
                    '<b>RESPONSABLE: </b>' + calEvent.responsable + '<br>' +
                    '<b>CENTRO COSTO: </b>  ' + calEvent.centrocosto + '<br>' +
                    '<b>SOLICITADO POR: </b>' + calEvent.solicitante + '<br>' +
                    '<b>INICIO: </b>      ' + s + '<br>' +
                    '<b>TERMINO: </b>     ' + e + '<br>' +
                    '<b>DESCRIPCIÓN: </b> ' + calEvent.description + '<br>' +
                    '<b>ADJUNTO: </b> ' + (calEvent.archivo ? calEvent.archivo : 'SIN ARCHIVO') + '<br> <hr>' +
                    '<b>COMENTARIO </b> ' + calEvent.comentario+
                    '<hr><b> ** Si desea anular la reserva debe enviar un correo a daravena@fabrimetal.cl <b>';

            var Botones2 = {cancel: {label: 'Cerrar', className: 'btn-info'}};

            if (calEvent.estado == 0) {
                Botones2 = {cancel: {label: 'Cerrar', className: 'btn-info'},
                    /*otro: {label: "Anular", className: 'btn-dark', callback: function () {
                            $("#fc_anular").trigger('click');
                            $("#idreserva2").val(calEvent.idreserva);
                        }}*/
                };
            }

            if (calEvent.estado == 1) {
                var f = new Date();
                var e1 = new Date(calEvent.end);
                if (e1.getTime() < f.getTime()) {
                    Botones2 = {cancel: {label: 'Cerrar', className: 'btn-info'}};
                } else {

                    if (calEvent.idempleado == $("#solicita1").val()) {
                        Botones2 = {cancel: {label: 'Cerrar', className: 'btn-info'},
                            /*otro: {label: "Anular", className: 'btn-dark', callback: function () {
                                    $("#fc_anular").trigger('click');
                                    $("#idreserva2").val(calEvent.idreserva);
                                }}*/
                        };
                    } else {
                        Botones2 = {cancel: {label: 'Cerrar', className: 'btn-info'}};
                    }

                }

            }
            if (calEvent.estado == 2 || calEvent.estado == 3) {
                if (calEvent.idempleado == $("#solicita1").val()) {
                    Botones2 = {cancel: {label: 'Cerrar', className: 'btn-info'},
                        otro: {label: 'copiar', className: 'btn-primary', callback: function () {
                                copia(calEvent.idreserva);
                            }}
                    };
                } else {
                    Botones2 = {cancel: {label: 'Cerrar', className: 'btn-info'}};
                }

            }

            bootbox.dialog({
                title: '<b>' + calEvent.title + (calEvent.estado == 2 ? ' - Rechazado' : (calEvent.estado == 3 ? ' - Anulado' : '')) + '</b>',
                message: event,
                buttons: Botones2
            });

            if (calEvent.url) {
                window.open(calEvent.url);
                return false;
            }

            calendar.fullCalendar('unselect');
        }
    });


}

function limpiar() {
    $("#title").val("");
    $("#descr").val("");
    $("#fechadesde").val("");
    $("#fechahasta").val("");
    $("#desde").val("");
    $("#hasta").val("");
    $("#item").val("");
    $("#item").selectpicker('refresh');
    $("#solicita").val("");
    $("#solicita").selectpicker('refresh');
    $("#centrocosto").val("");
    $("#centrocosto").selectpicker('refresh');
    $("#responsable").val("");
    $("#responsable").selectpicker('refresh');
    $("#direccion").val("");
    $("#recibe").val("");
    $("#telefono").val("");
    $("#contactoobra").val("");
    $("#fonoobra").val("");
    $("#excel").val("");
    $("#treserva").val(0);
    $("#responsable").selectpicker('refresh');

    $("#title2").val("");
    $("#descr2").val("");
    $("#fechadesde2").val("");
    $("#fechahasta2").val("");
    $("#desde2").val("");
    $("#hasta2").val("");
    $("#item2").val("");
    $("#item2").selectpicker('refresh');
    $("#solicita2").val("");
    $("#solicita2").selectpicker('refresh');
    $("#coment2").val("");


}

function agregar() {
    $("#telefono").inputmask('remove');
    $("#fonoobra").inputmask('remove');

    var descrp = $("#descr").val();
    var desde = $("#fechadesde").val();
    var hasta = $("#fechahasta").val();
    var horaD = $("#desde").val();
    var horaH = $("#hasta").val();
    var item = $("#item").val();
    var solicita = $("#solicita").val() ? $("#solicita").val() : $("#solicita1").val();
    var centrocosto = $("#centrocosto").val();
    var combo = document.getElementById("item");
    var itemtext = combo.options[combo.selectedIndex].text;
    var mensaje = descrp + " <br> <b>DIRECCIÓN: </b>" + $("#direccion").val()
            + " <br> <b>Contacto en obra (Fabrimetal):</b> " + $("#recibe").val()
            + " <br> <b>Fono contacto:</b> " + $("#telefono").val()
            + " <br> <b>Contacto de  obra: </b>" + $("#contactoobra").val()
            + " <br> <b>Fono contacto obra:</b> " + $("#fonoobra").val();

    var treserva = $("#treserva").val();
    var file = document.getElementById("excel").files[0];

    var responsable = $("#responsable").val();

    
    var formData = new FormData();
    formData.append("item", item);
    formData.append("desde", desde);
    formData.append("hasta", hasta);
    formData.append("horadesde", horaD);
    formData.append("horahasta", horaH);
    formData.append("estado", 1);
    formData.append("descripcion", mensaje.replace(/[']/g, ''));
    formData.append("nombre", itemtext);
    formData.append("solicita", solicita);
    formData.append("centrocosto", centrocosto);
    formData.append("responsable", responsable);
    formData.append("treserva", treserva);
    formData.append("archivo", file);

    $.ajax({

        url: '../ajax/reservaAjax.php?op=solicitar',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,

        success: function (data) {
            if (!isNaN(data)) {
                if (data != 0) {
                    $.post("../ajax/reservaAjax.php?op=email", {idreserva: data, texto: "AGENDADA"}, function (data2) {});
                    $.post("../ajax/reservaAjax.php?op=emailresponsable", {idreserva: data}, function (data2) {});

                    new PNotify({
                        title: 'Correcto!',
                        text: 'Reserva guardada.',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                    $(".close").click();
                } else {
                    new PNotify({
                        title: 'Error!',
                        text: 'Reserva no guardada',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                }

            } else {
                new PNotify({
                    title: 'Error!',
                    text: 'Reserva no guardada,' + itemtext + ' ocupada(o)' + data,
                    type: 'error',
                    styling: 'bootstrap3'
                });
            }
        }
    });

    limpiar();
    listar();
}

function editar() {

    if ($("#item2").val() == "" || $("#item2").val() == 0) {
        new PNotify({
            title: 'OPS!',
            text: 'Debes seleccionar un item a reservar.',
            type: 'info',
            styling: 'bootstrap3'
        });
        return false;
    }

    if ($("#responsable2").val() == "" || $("#responsable2").val() == 0 || $("#responsable2").val() == null) {
        new PNotify({
            title: 'OPS!',
            text: 'Debes seleccionar un responsable.',
            type: 'info',
            styling: 'bootstrap3'
        });
        return false;
    }

    var title = $("#title").val();
    var descrp = $("#descr2").val();
    var desde = $("#fechadesde2").val();
    var hasta = $("#fechahasta2").val();
    var horaD = $("#desde2").val();
    var horaH = $("#hasta2").val();
    var item = $("#item2").val();
    var idreserva = $("#reserva2").val();
    var solicita = $("#solicita2").val();
    var combo = document.getElementById("item2");
    var itemtext = combo.options[combo.selectedIndex].text;
    var centrocosto = $("#centrocosto2").val();
    var comentario = $("#coment2").val();
    var responsable = $("#responsable2").val();
    var treserva = $("#treserva1").val();

    $.post("../ajax/reservaAjax.php?op=grabaryeditar", {"solicita": solicita, "idreserva": idreserva, "item": item, "desde": desde, "hasta": hasta,
        "horadesde": horaD, "horahasta": horaH,
        "descripcion": descrp, "nombre": itemtext, "estado": 1, comentario: comentario, "centrocosto": centrocosto, "responsable": responsable, "treserva": treserva},
            function (data) {
                if (!isNaN(data)) {
                    if (data != 0) {
                        $.post("../ajax/reservaAjax.php?op=email", {idreserva: idreserva, texto: "AGENDADA"}, function (data2) {});
                        $.post("../ajax/reservaAjax.php?op=emailresponsable", {idreserva: idreserva}, function (data2) {});
                        new PNotify({
                            title: 'Correcto!',
                            text: 'Reserva guardada.',
                            type: 'success',
                            styling: 'bootstrap3'
                        });
                        $(".close").click();
                    } else {
                        new PNotify({
                            title: 'Error!',
                            text: 'Reserva no guardada',
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }

                } else {
                    new PNotify({
                        title: 'Error!',
                        text: 'Reserva no guardada,' + itemtext + ' ocupada(o)' + data,
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                }
            }
    );

    limpiar();

    listar();
    $(".close").click();
}

function solicitar() {
    $("#telefono").inputmask('remove');
    $("#fonoobra").inputmask('remove');

    var descrp = $("#descr").val();
    var desde = $("#fechadesde").val();
    var hasta = $("#fechahasta").val();
    var horaD = $("#desde").val();
    var horaH = $("#hasta").val();
    var item = $("#item").val();
    var solicita = $("#solicita").val() ? $("#solicita").val() : $("#solicita1").val();
    var centrocosto = $("#centrocosto").val();
    var combo = document.getElementById("item");
    var itemtext = combo.options[combo.selectedIndex].text;
    var mensaje = descrp + " <br> <b>DIRECCIÓN: </b>" + $("#direccion").val()
            + " <br> <b>Contacto en obra (Fabrimetal):</b> " + $("#recibe").val()
            + " <br> <b>Fono contacto:</b> " + $("#telefono").val()
            + " <br> <b>Contacto de  obra: </b>" + $("#contactoobra").val()
            + " <br> <b>Fono contacto obra:</b> " + $("#fonoobra").val();

    var treserva = $("#treserva").val();
    var file = document.getElementById("excel").files[0];

    var form_valido = false;

    if ($.trim($("#descr").val()) != "") {

        if ($.trim($("#descr").val()).length > 30) {

            form_valido = true;

        } else {

            new PNotify({
                title: 'Error!',
                text: 'La Descripcion debe ser tener mas de 30 caracteres.',
                type: 'error',
                styling: 'bootstrap3'
            });

            form_valido = false;

        }

    } else {
        form_valido = true;
    }


    var formData = new FormData();
    formData.append("item", item);
    formData.append("desde", desde);
    formData.append("hasta", hasta);
    formData.append("horadesde", horaD);
    formData.append("horahasta", horaH);
    formData.append("descripcion", mensaje.replace(/[']/g, ''));
    formData.append("nombre", itemtext);
    formData.append("solicita", solicita);
    formData.append("estado", 0);
    formData.append("centrocosto", centrocosto);
    formData.append("treserva", treserva);
    formData.append("archivo", file);

    if (form_valido) {
        $.ajax({

            url: '../ajax/reservaAjax.php?op=solicitar',
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,

            success: function (data) {

                if (!isNaN(data)) {
                    if (data != 0) {
                        new PNotify({
                            title: 'Correcto!',
                            text: 'Reserva guardada.',
                            type: 'success',
                            styling: 'bootstrap3'
                        });
                        $(".close").click();
                    } else {
                        new PNotify({
                            title: 'Error!',
                            text: 'Reserva no guardada',
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }

                } else {
                    new PNotify({
                        title: 'Error!',
                        text: 'Reserva no guardada,' + itemtext + ' ocupada(o)' + data,
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                }
            }

        });
        /*$.post("../ajax/reservaAjax.php?op=solicitar", formData,
         function (data) {
         if (!isNaN(data)) {
         if (data != 0) {
         new PNotify({
         title: 'Correcto!',
         text: 'Reserva guardada.',
         type: 'success',
         styling: 'bootstrap3'
         });
         $(".close").click();
         } else {
         new PNotify({
         title: 'Error!',
         text: 'Reserva no guardada',
         type: 'error',
         styling: 'bootstrap3'
         });
         }
         
         } else {
         new PNotify({
         title: 'Error!',
         text: 'Reserva no guardada,' + itemtext + ' ocupada(o)' + data,
         type: 'error',
         styling: 'bootstrap3'
         });
         }
         }
         );   */
        limpiar();
        listar();
        $(".close").click();
    }

}


function copia(idreserva) {
    $("#fc_copiar").trigger('click');

    $.post("../ajax/reservaAjax.php?op=mostrar", {idreserva: idreserva}, function (data) {
        data = JSON.parse(data);

        $("#idservicio_copia").val(idreserva);

        html = '<b>RESERVA: </b>   TRANSPORTE <br>' +
                '<b>CENTRO COSTO: </b>  ' + data.centrocosto + '<br>' +
                '<b>SOLICITADO POR: </b>' + data.nomb + '<br>' +
                '<b>DESCRIPCIÓN: </b> ' + data.descripcion + '<br>' +
                '<b>ADJUNTO: </b> ' + (data.file ? '<a href="../files/transporte/' + data.file + '" target="_blank"><img src="../files/icono/Excel-icon.png" height="30px" width="30px" ></a>' : 'SIN ARCHIVO') + '<br>';

        $("#contenido").html(html);
    });
}

function limpiarCopia() {
    $("#idservicio_copia").val("");
    $("#fechadesde_copia").val("");
    $("#fechahasta_copia").val("");
    $("#desde_copia").val("");
    $("#hasta_copia").val("");
    $("excel1").val("");
}

function solicitaCopia() {

    var idreserva = $("#idservicio_copia").val();
    var desde = $("#fechadesde_copia").val();
    var hasta = $("#fechahasta_copia").val();
    var horadesde = $("#desde_copia").val();


    var d = new Date(desde + ' ' + horadesde);
    var f = new Date();
    if (d.getDate() < f.getDate()) {
        new PNotify({
            title: 'OPS!',
            text: 'No puede reservar una fecha menor a hoy.',
            type: 'info',
            styling: 'bootstrap3'
        });
        return false;
    }

    $.post("../ajax/reservaAjax.php?op=mostrar", {idreserva: idreserva}, function (data) {
        data = JSON.parse(data);
        var file = document.getElementById("excel1").files[0];
        var horaD = $("#desde_copia").val();
        var horaH = $("#hasta_copia").val();


        var formData = new FormData();
        formData.append("item", 0);
        formData.append("desde", desde);
        formData.append("hasta", hasta);
        formData.append("horadesde", horaD);
        formData.append("horahasta", horaH);
        formData.append("descripcion", data.descripcion.replace(/[']/g, ''));
        formData.append("nombre", 'TRANSPORTE');
        formData.append("solicita", data.solicitante);
        formData.append("estado", 0);
        formData.append("centrocosto", data.centrocosto);
        formData.append("treserva", data.treserva);
        formData.append("archivo", file);
        formData.append("file", data.file);



        $.ajax({

            url: '../ajax/reservaAjax.php?op=solicitar',
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,

            success: function (data) {

                if (!isNaN(data)) {
                    if (data != 0) {
                        new PNotify({
                            title: 'Correcto!',
                            text: 'Reserva guardada.',
                            type: 'success',
                            styling: 'bootstrap3'
                        });
                        $("#cierracopia").click();
                        calendario();
                        listar();
                    } else {
                        new PNotify({
                            title: 'Error!',
                            text: 'Reserva no guardada' + data,
                            type: 'error',
                            styling: 'bootstrap3'
                        });
                    }

                } else {
                    new PNotify({
                        title: 'Error!',
                        text: 'Reserva no guardada, TRANSPORTE ocupada(o)' + data,
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                }
            }

        });
        limpiarCopia();
        //listar();
        $("#cierracopia").click();


    });
}

function listar() {
    var events = [];

    $('#calendario').fullCalendar('removeEvents', function () {
        return true;
    });
    $('#calendario2').fullCalendar('removeEvents', function () {
        return true;
    });

    $('#calendario').fullCalendar('refetchEvents');
    $('#calendario2').fullCalendar('refetchEvents');

    $.post("../ajax/reservaAjax.php?op=lista", function (data) {
        data = JSON.parse(data);

        for (i = 0; i < data.length; i++) {
            events.push({
                title: data[i]['nombitem'] + (data[i]['estado'] == 2 ? " - (R)" : "") + " - " + data[i]['nombresponsable'],
                start: data[i]['desde'],
                end: data[i]['hasta'],
                allDay: false,
                color: data[i]['color'], // an option!
                textColor: 'white',
                description: data[i]['descripcion'],
                idreserva: data[i]['idreserva'],
                solicitante: data[i]['nomb'],
                idempleado: data[i]['solicitante'],
                comentario: data[i]['comentario'],
                estado: data[i]['estado'],
                centrocosto: data[i]['centrocosto'],
                responsable: data[i]['nombresponsable'],
                archivo: (data[i]['file'] ? '<a href="../files/transporte/' + data[i]['file'] + '" target="_blank"><img src="../files/icono/Excel-icon.png" height="30px" width="30px" ></a>' : ""),
                treserva: (data[i]['treserva'] == '1' ? 'RETIRO' : 'DESPACHO'),
                diahoradesde: data[i]['desde'] + ' ' + data[i]['horadesde'],
                diahorahasta: data[i]['hasta'] + ' ' + data[i]['horahasta'],
                created_time: data[i]['created_time']
                        //url: '../ajax/reservaAjax.php?op=lista;'
            });
        }
        $('#calendario').fullCalendar('addEventSource', events);
        $('#calendario2').fullCalendar('addEventSource', events);
        notificacion();
    });


}

function listarEstado(estado, solicita) {
    var events = [];

    $('#calendario').fullCalendar('removeEvents', function () {
        return true;
    });
    $('#calendario2').fullCalendar('removeEvents', function () {
        return true;
    });

    $('#calendario').fullCalendar('refetchEvents');
    $('#calendario2').fullCalendar('refetchEvents');

    $.get("../ajax/reservaAjax.php?op=listaxEstado", {estado: estado, solicita: solicita}, function (data) {
        data = JSON.parse(data);

        for (i = 0; i < data.length; i++) {
            events.push({
                title: data[i]['nombitem'] + (data[i]['estado'] == 2 ? " - (R)" : (data[i]['estado'] == 3 ? " - (A)" : "")) + " - " + data[i]['nombresponsable'],
                start: data[i]['desde'],
                end: data[i]['hasta'],
                allDay: false,
                color: data[i]['color'], // an option!
                textColor: 'white',
                description: data[i]['descripcion'],
                idreserva: data[i]['idreserva'],
                solicitante: data[i]['nomb'],
                idempleado: data[i]['solicitante'],
                comentario: data[i]['comentario'],
                estado: data[i]['estado'],
                centrocosto: data[i]['centrocosto'],
                responsable: data[i]['nombresponsable'],
                archivo: (data[i]['file'] ? '<a href="../files/transporte/' + data[i]['file'] + '" target="_blank"><img src="../files/icono/Excel-icon.png" height="30px" width="30px" ></a>' : ""),
                treserva: (data[i]['treserva'] == '1' ? 'RETIRO' : 'DESPACHO'),
                created_time: data[i]['created_time']
                        //url: '../ajax/reservaAjax.php?op=lista;'
            });
        }
        $('#calendario').fullCalendar('addEventSource', events);
        $('#calendario2').fullCalendar('addEventSource', events);
        notificacion();
    });
}

function eliminar(idreserva2, sw) {
    var comentario = "";
    if (sw) {
        comentario = "Rechazado por administrador.";
    } else {
        if ($("#coment2").val() == "") {
            new PNotify({
                title: 'OPS!',
                text: 'Debe ingresar un comentario para rechazar e informar sobre esta reserva al solicitante.',
                type: 'info',
                styling: 'bootstrap3'
            });
            return false;
        }
        comentario = $("#coment2").val();
    }

    var idreserva = 0;

    if (idreserva2) {
        idreserva = idreserva2;
    } else {
        idreserva = $("#reserva2").val();
    }

    bootbox.confirm("Esta seguro que quiere rechazar esta reserva?", function (result) {
        if (result) {
            $.post("../ajax/reservaAjax.php?op=eliminar", {idreserva: idreserva, comentario: comentario}, function (e) {
                $("#coment2").val("");
                $(".close").click();
                if (e == 1) {
                    $.post("../ajax/reservaAjax.php?op=email", {idreserva: idreserva, texto: "RECHAZADA"}, function (data2) {});
                    new PNotify({
                        title: 'Correcto!',
                        text: 'Reserva Rechazada.',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                } else {
                    new PNotify({
                        title: 'Error!',
                        text: 'Reserva no rechazada',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                }
                listar();

            });
        }
    });

}


function anular() {
    var comentario = "";
    var idreserva = $("#idreserva2").val();

    var comentario = "";

    if ($("#comentuser").val() != "") {
        comentario = "<b>SOLICITANTE:</b> " + $("#comentuser").val();
    } else if ($("#comentadmin").val() != "") {
        comentario = "<b>ADMINISTRADOR:</b> " + $("#comentadmin").val();
    }

    bootbox.confirm("Esta seguro que quiere anular esta reserva?", function (result) {
        if (result) {
            $.post("../ajax/reservaAjax.php?op=anular", {idreserva: idreserva, comentario: comentario}, function (e) {
                $("#coment2").val("");
                //$("#CalenderModalAnular").hide();
                $(".close").trigger('click');
                if (e == 1) {
                    $.post("../ajax/reservaAjax.php?op=email", {idreserva: idreserva, texto: "ANULADA"}, function (data2) {});
                    new PNotify({
                        title: 'Correcto!',
                        text: 'Reserva Anulada.',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
                } else {
                    new PNotify({
                        title: 'Error!',
                        text: 'Reserva no anulada',
                        type: 'error',
                        styling: 'bootstrap3'
                    });
                }
                listar();

            });
        }
    });

    $("#idreserva2").val("");
    $("#motivoanula").val("");
    $("#motivoanula").selectpicker('refresh');
    $("#comentuser").val("");
    $("#comentadmin").val("");
}

function mostrar(idreserva) {

    $('#fc_edit').click();


    $.post("../ajax/reservaAjax.php?op=mostrar", {idreserva: idreserva}, function (data) {
        data = JSON.parse(data);
        
        $("#created_time").empty();
        $("#created_time").append("Fecha creación: " + formatofecha(data.created_time));
        
        $("#descr2").val(data.descripcion);
        $("#descr3").html(data.descripcion);
        $("#fechadesde2").val(data.desde);
        $("#fechahasta2").val(data.hasta);
        $("#fechadesde4").html(formatofechacorta(data.desde));
        $("#fechahasta4").html(formatofechacorta(data.hasta));
        $("#reserva2").val(idreserva);


        $("#desde2").val(data.horadesde);
        $("#hasta2").val(data.horahasta);

        $.post("../ajax/itemAjax.php?op=selectitem", function (r) {
            $("#item2").html(r);
            $("#item2").val(data.item);
            $("#item2").selectpicker('refresh');
        });

        $("#treserva1").val(data.treserva);
        $("#treserva1").selectpicker('refresh');

        $("#adjunto").html('<a href="../files/transporte/' + data.file + '" target="_blank"><img src="../files/icono/Excel-icon.png" height="30px" width="30px" ></a>');

        $.post("../ajax/empleadoAjax.php?op=selectempleado", function (r) {
            $("#solicita2").html(r);
            $("#solicita2").val(data.solicitante);
            $("#solicita2").selectpicker('refresh');
        });

        $.post("../ajax/responsable.php?op=selectresponsable", function (r) {
            $("#responsable2").html(r);
            $("#responsable2").val(data.responsable);
            $("#responsable2").selectpicker('refresh');
        });

        $.post("https://www.fabrimetalsa.cl/appfabrimetal/ajax/ApiExterna.php?apicall=SelectCentrosCosto", function (x) {
            data2 = JSON.parse(x);
            $("#centrocosto2").html(data2.contenido);
            $("#centrocosto2").val(data.centrocosto);
            $("#centrocosto2").selectpicker('refresh');
        });


    });


}

function rechazados() {
    var events = [];

    $('#calendario').fullCalendar('removeEvents', function () {
        return true;
    });
    $('#calendario2').fullCalendar('removeEvents', function () {
        return true;
    });

    $('#calendario').fullCalendar('refetchEvents');
    $('#calendario2').fullCalendar('refetchEvents');

    $.post("../ajax/reservaAjax.php?op=listarechazados", function (data) {
        data = JSON.parse(data);

        for (i = 0; i < data.length; i++) {
            events.push({
                title: data[i]['nombitem'] + (data[i]['estado'] == 2 ? " - (R)" : "") + " - " + data[i]['nombresponsable'],
                start: data[i]['desde'],
                end: data[i]['hasta'],
                allDay: false,
                color: data[i]['color'], // an option!
                textColor: 'white',
                description: data[i]['descripcion'],
                idreserva: data[i]['idreserva'],
                solicitante: data[i]['nomb'],
                idempleado: data[i]['solicitante'],
                comentario: data[i]['comentario'],
                estado: data[i]['estado'],
                centrocosto: data[i]['centrocosto'],
                responsable: data[i]['responsable'],
                archivo: (data[i]['file'] ? '<a href="../files/transporte/' + data[i]['file'] + '" target="_blank"><img src="../files/icono/Excel-icon.png" height="30px" width="30px" ></a>' : ""),
                treserva: (data[i]['treserva'] == '1' ? 'RETIRO' : 'DESPACHO')
                        //url: '../ajax/reservaAjax.php?op=lista;'
            });
        }
        $('#calendario').fullCalendar('addEventSource', events);
        $('#calendario2').fullCalendar('addEventSource', events);
        notificacion();
    });

}

function Listartodos() {
    var events = [];

    $('#calendario').fullCalendar('removeEvents', function () {
        return true;
    });
    $('#calendario2').fullCalendar('removeEvents', function () {
        return true;
    });

    $('#calendario').fullCalendar('refetchEvents');
    $('#calendario2').fullCalendar('refetchEvents');

    $.post("../ajax/reservaAjax.php?op=listaTodos", function (data) {
        data = JSON.parse(data);

        for (i = 0; i < data.length; i++) {
            events.push({
                title: data[i]['nombitem'] + (data[i]['estado'] == 2 ? " - (R)" : "") + " - " + data[i]['nombresponsable'],
                start: data[i]['desde'],
                end: data[i]['hasta'],
                allDay: false,
                color: data[i]['color'], // an option!
                textColor: 'white',
                description: data[i]['descripcion'],
                idreserva: data[i]['idreserva'],
                solicitante: data[i]['nomb'],
                idempleado: data[i]['solicitante'],
                comentario: data[i]['comentario'],
                estado: data[i]['estado'],
                centrocosto: data[i]['centrocosto'],
                responsable: data[i]['responsable'],
                archivo: (data[i]['file'] ? '<a href="../files/transporte/' + data[i]['file'] + '" target="_blank"><img src="../files/icono/Excel-icon.png" height="30px" width="30px" ></a>' : ""),
                treserva: (data[i]['treserva'] == '1' ? 'RETIRO' : 'DESPACHO'),
                created_time: data[i]['created_time']
                        //url: '../ajax/reservaAjax.php?op=lista;'
            });
        }
        $('#calendario').fullCalendar('addEventSource', events);
        $('#calendario2').fullCalendar('addEventSource', events);
        notificacion();
    });

}

function rechazadostodos() {
    var events = [];

    $('#calendario').fullCalendar('removeEvents', function () {
        return true;
    });
    $('#calendario2').fullCalendar('removeEvents', function () {
        return true;
    });

    $('#calendario').fullCalendar('refetchEvents');
    $('#calendario2').fullCalendar('refetchEvents');

    $.post("../ajax/reservaAjax.php?op=listartodosrechazados", function (data) {
        data = JSON.parse(data);

        for (i = 0; i < data.length; i++) {
            events.push({
                title: data[i]['nombitem'] + " - " + data[i]['nombresponsable'],
                start: data[i]['desde'],
                end: data[i]['hasta'],
                allDay: false,
                color: data[i]['color'], // an option!
                textColor: 'white',
                description: data[i]['descripcion'],
                idreserva: data[i]['idreserva'],
                solicitante: data[i]['nomb'],
                idempleado: data[i]['solicitante'],
                comentario: data[i]['comentario'],
                estado: data[i]['estado'],
                centrocosto: data[i]['centrocosto'],
                responsable: data[i]['responsable'],
                archivo: (data[i]['file'] ? '<a href="../files/transporte/' + data[i]['file'] + '" target="_blank"><img src="../files/icono/Excel-icon.png" height="30px" width="30px" ></a>' : ""),
                treserva: (data[i]['treserva'] == '1' ? 'RETIRO' : 'DESPACHO')
                        //url: '../ajax/reservaAjax.php?op=lista;'
            });
        }
        $('#calendario').fullCalendar('addEventSource', events);
        $('#calendario2').fullCalendar('addEventSource', events);
        notificacion();
    });

}

function validar_fechas() {

    if ($('#hasta').val() < $('#desde').val()) {

        $("#enviar").prop("disabled", true);

        new PNotify({
            title: 'Error!',
            text: 'La hora HASTA no puede ser menor que DESDE.',
            type: 'error',
            styling: 'bootstrap3'
        });

        $("#div_desde").addClass("has-error");
        $("#div_hasta").addClass("has-error");

    } else {
        $("#enviar").prop("disabled", false);
        $("#div_desde").removeClass("has-error");
        $("#div_hasta").removeClass("has-error");
    }


    if ($("#fechahasta_copia").val() >= $("#fechadesde_copia").val()) {
        if ($('#hasta_copia').val() < $('#desde_copia').val()) {

            $("#enviar_copia").prop("disabled", true);

            new PNotify({
                title: 'Error!',
                text: 'La hora HASTA no puede ser menor que DESDE.',
                type: 'error',
                styling: 'bootstrap3'
            });

            $("#div_desde_copia").addClass("has-error");
            $("#div_hasta_copia").addClass("has-error");

        } else {
            $("#enviar_copia").prop("disabled", false);
            $("#div_desde_copia").removeClass("has-error");
            $("#div_hasta_copia").removeClass("has-error");
            $("#div_fecha_desde").removeClass("has-error");
            $("#div_fecha_hasta").removeClass("has-error");
        }
    }else{
        $("#enviar_copia").prop("disabled", true);

        new PNotify({
            title: 'Error!',
            text: 'La Fecha HASTA no puede ser menor que DESDE.',
            type: 'error',
            styling: 'bootstrap3'
        });
        
        $("#div_fecha_desde").addClass("has-error");
        $("#div_fecha_hasta").addClass("has-error");
    }


}

/**
 * le da formato a una fecha
 * @param {string} texto
 * @returns {String}
 */
function formatofecha(texto) {
    if (texto != "") {
        if (texto != null) {
            var fecha = texto.split(" ");
            var info = fecha[0].split('-');
            return info[2] + '/' + info[1] + '/' + info[0] + ' ' + fecha[1];
        }
    }
}

init();
