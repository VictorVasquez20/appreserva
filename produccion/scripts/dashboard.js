/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function init() {
    calendar();
    //listar();
    setInterval("listar()", 60000);
    
}

function calendar() {

    if (typeof ($.fn.fullCalendar) === 'undefined') {
        return;
    }
    $('#calendario').fullCalendar('removeEvents', function () {
        return true;
    });

    $('#calendario').fullCalendar('refetchEvents');
    console.log('calendar');

    var date = new Date(),
            d = date.getDate(),
            m = date.getMonth(),
            y = date.getFullYear(),
            started,
            categoryClass;

    var calendar = $('#calendario').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay,listMonth'
        },
        locale: 'es',
        /* EDITAR */
        editable: true,
        selectable: true,
        /* EDITAR */
        eventLimit: false,
        selectHelper: false,
        contentHeight: 880,
        events: {
            url: '../ajax/reservaAjax.php',
            method: 'GET',
            data:function () { // a function that returns an object
                return {
                    op: 'listadashboard',
                    start: $('#calendario').fullCalendar('getView').start,
                    end: $('#calendario').fullCalendar('getView').end
                };
            },
        },
        select: function (start, end, allDay) {
            var s = new Date(start);
            s.setDate(s.getDate() + 1);
            var e = new Date(end);
            var f = new Date();

            if (s.getTime() < f.getTime()) {
                bootbox.alert("No puede reservar una fecha menor a hoy");
                return false;
            }

            $('#fc_create').click();

            $("#fechadesde").val(s.getFullYear() + "-" + (s.getMonth() + 1) + "-" + (s.getDate()));
            $("#fechahasta").val(e.getFullYear() + "-" + (e.getMonth() + 1) + "-" + (e.getDate()));
            $("#fechadesde3").html((s.getDate()) + "-" + (s.getMonth() + 1) + "-" + s.getFullYear());
            $("#fechahasta3").html((e.getDate()) + "-" + (e.getMonth() + 1) + "-" + e.getFullYear());

            started = start;
            ended = end;
            $(".antosubmit").on("click", function () {
                //categoryClass = $("#event_type").val();
                calendar.fullCalendar('unselect');
                $('.antoclose').click();
                return false;

            });
        },
        eventClick: function (calEvent, jsEvent, view) {
            var s = new Date(calEvent.start);
            var e = new Date(calEvent.end);
            s = s.getUTCDate() + "-" + (s.getUTCMonth() + 1) + "-" + s.getUTCFullYear() + " a las " + s.getUTCHours() + ":" + s.getMinutes() + " Hrs.";
            e = e.getUTCDate() + "-" + (e.getUTCMonth() + 1) + "-" + e.getUTCFullYear() + " a las " + e.getUTCHours() + ":" + e.getMinutes() + " Hrs.";


            var event = '<b>RESERVA: </b>     ' + calEvent.title + '<br>' +
                    '<b>RESPONSABLE: </b>' + calEvent.responsable + '<br>' +
                    '<b>CENTRO COSTO: </b>' + calEvent.centrocosto + '<br>' +
                    '<b>SOLICITADO POR: </b>' + calEvent.solicitante + '<br>' +
                    '<b>INICIO: </b>      ' + s + '<br>' +
                    '<b>TERMINO: </b>     ' + e + '<br>' +
                    '<b>DESCRIPCIÓN: </b> ' + calEvent.description + '<br> <hr>' +
                    '<b>COMENTARIO </b> ' + calEvent.comentario;
            
            var Botones2 = {cancel: { label: 'Cerrar', className: 'btn-info'}};
            
            if(calEvent.estado == 1){
                Botones2 = {cancel: { label: 'Cerrar', className: 'btn-info'},
                           ok: {label: "rechazar",className: 'btn-danger', callback: function () {eliminar(calEvent.idreserva, true);}},
                           otro: {label: "Anular",className: 'btn-dark',callback: function () {$("#fc_anular").trigger('click'); $("#idreserva2").val(calEvent.idreserva);}}
                       };
            }
            
            bootbox.dialog({
                title: '<b>' + calEvent.title + '</b>',
                inputType: 'text',
                message: event,
                buttons: Botones2

            });

            if (calEvent.url) {
                window.open(calEvent.url);
                return false;
            }

            calendar.fullCalendar('unselect');
        }
    });

}

function listar() {
    var events = [];

    $('#calendario').fullCalendar('removeEvents', function () {
        return true;
    });

    $('#calendario').fullCalendar('refetchEvents');

    $.get("../ajax/reservaAjax.php?op=listadashboard", function (data) {
        data = JSON.parse(data);

        for (i = 0; i < data.length; i++) {
            events.push({
                title: data[i]['nombitem'] + (data[i]['estado'] == 2 ? " - (R)" : (data[i]['estado'] == 3 ? " - (A)" : "")),
                start: data[i]['desde'],
                end: data[i]['hasta'],
                allDay: false,
                color: data[i]['color'], // an option!
                textColor: 'white',
                description: data[i]['descripcion'],
                idreserva: data[i]['idreserva'],
                solicitante: data[i]['nomb'],
                comentario: data[i]['comentario'],
                estado: data[i]['estado'],
                centrocosto: data[i]['centrocosto'],
                responsable: data[i]['responsable']
                        //url: '../ajax/reservaAjax.php?op=lista;'
            });
        }
        $('#calendario').fullCalendar('addEventSource', events);
    });


}
init();