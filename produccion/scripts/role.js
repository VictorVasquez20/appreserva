var tabla;

//funcion que se ejecuta iniciando
function init(){
	mostrarform(false);
	listar();

	$.post("../ajax/roleAjax.php?op=permisos&id=0", function(r){
		$("#permisos").html(r);
	});

	$("#formulario").on("submit", function(e){
		guardaryeditar(e);
	});
}


// Otras funciones
function limpiar(){

	$("#idrole").val("");	
	$("#nombre").val("");
	$("#descripcion").val("");

}

function mostrarform(flag){

	limpiar();
	if(flag){
		$("#listadoroles").hide();
		$("#formularioroles").show();
		$("#op_agregar").hide();
		$("#op_listar").show();
		$("#btnGuardar").prop("disabled", false);

	}else{
		$("#listadoroles").show();
		$("#formularioroles").hide();
		$("#op_agregar").show();
		$("#op_listar").hide();
	}

}

function cancelarform(){
	limpiar();
	mostrarform(false);
}

function listar(){
	tabla=$('#tblroles').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:Botones,
                "language": Español,
		"ajax":{
			url:'../ajax/roleAjax.php?op=listar',
			type:"get",
			dataType:"json",
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 10, //Paginacion 10 items
		"order" : [[0 , "desc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();
}

function guardaryeditar(e){
	e.preventDefault();
	$("#btnGuardar").prop("disabled", true);
	var formData = new FormData($("#formulario")[0]);
	
	$.ajax({
		url:'../ajax/roleAjax.php?op=guardaryeditar',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
			bootbox.alert(datos);
			mostrarform(false);
			tabla.ajax.reload();
		}
	});
	limpiar();
}

function mostar(idrole){
	$.post("../ajax/roleAjax.php?op=mostar",{idrole:idrole}, function(data,status){
		data = JSON.parse(data);
		mostrarform(true);
		$("#idrole").val(data.idrole);	
		$("#nombre").val(data.nombre);
		$("#descripcion").val(data.descripcion);
	});

	$.post("../ajax/roleAjax.php?op=permisos&id="+idrole, function(r){
		$("#permisos").html(r);
	});
}

function eliminar(idrole){

	bootbox.confirm("Esta seguro que quiere eliminar el role?", function(result){
		if(result){
			$.post("../ajax/roleAjax.php?op=eliminar",{idrole:idrole}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			});	
		}
	});
}


init();