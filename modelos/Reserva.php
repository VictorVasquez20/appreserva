<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Reserva
 *
 * @author aaron
 */
require_once '../config/conexion.php';
require_once '../modelos/Empleado.php';
require_once '../modelos/Responsable.php';

require_once '../public/build/lib/PHPMailer/class.phpmailer.php';
require_once '../public/build/lib/PHPMailer/class.smtp.php';

class Reserva {

    public $idreserva;
    public $usuario;
    public $item;
    public $solicitante;
    public $desde;
    public $hasta;
    public $horadesde;
    public $horahasta;
    public $descripcion;
    public $nombre;

    function __construct() {
        
    }

    function insertar($iduser, $item, $desde, $hasta, $horadesde, $horahasta, $descripcion, $nombre, $solicita, $estado, $comentario, $centrocosto, $responsable, $treserva, $archivo) {
        $sw = "";
        if ($item != 0) {
            $sw = $this->verifica($item, $desde, $horadesde, $responsable);
        }

        if (empty($sw)) {
            $sql = "INSERT INTO `reserva`(`iduser`, `item`, `desde`, `hasta`, `horadesde`, `horahasta`, `descripcion`, `nombre`, `solicitante`,`estado`, `comentario` , `centrocosto`, `idresponsable`, treserva, file) "
                    . "VALUES ($iduser, $item, '$desde', '$hasta', '$horadesde', '$horahasta', '$descripcion','$nombre', $solicita, $estado, '$comentario', '$centrocosto', $responsable, $treserva, '$archivo')";

            //var_dump($sql);

            return ejecutarConsulta_retornarID($sql);
        } else {
            $hora = $sw['horadesde'] . " a las " . $sw['horahasta'];
            $fecha = date("d/m/Y", strtotime($sw['desde'])) . " Hasta " . date("d/m/Y", strtotime($sw['hasta']));

            return " las fechas " . $fecha . " desde " . $hora . " por: " . $sw["solicitante"];
        }
    }

    function editar($idreserva, $iduser, $item, $desde, $hasta, $horadesde, $horahasta, $descripcion, $nombre, $solicitante, $estado, $comentario, $centrocosto, $responsable, $treserva) {

        $sql = "UPDATE `reserva` SET "
                . "`iduser`= $iduser,"
                . "`item`= $item,"
                . "`desde`= '$desde',"
                . "`hasta`= '$hasta',"
                . "`horadesde`= '$horadesde',"
                . "`horahasta`= '$horahasta',"
                . "`descripcion`= '$descripcion',"
                . "`nombre`= '$nombre',"
                . "`solicitante`= $solicitante,"
                . "`estado`= $estado,"
                . "`comentario`= '$comentario', "
                . "`centrocosto`= '$centrocosto', "
                . "`idresponsable` = $responsable ,"
                . "`update_time`= CURRENT_TIMESTAMP,"
                . "`update_user`= $iduser, "
                . " treserva = $treserva "
                . "WHERE `idreserva`= $idreserva";


        return ejecutarConsulta($sql);
    }

    function mostrar($idreserva) {
        $sql = "select r.*, i.nombre as 'nombItem', i.color, concat(e.nombre, ' ' , e.apellido) as 'nomb', iFNULL(re.nombre, '') as 'responsable' "
                . "from reserva r "
                . "INNER JOIN Item_reserva i on i.iditem = r.item "
                . "LEFT JOIN empleado e on e.idempleado = r.solicitante "
                . "LEFT JOIN responsable_reserva re on re.idresponsable = r.idresponsable "
                . "Where idreserva = $idreserva";
        return ejecutarConsultaSimpleFila($sql);
    }

    function listar($estado, $iduser) {
        $sql = "select r.*, i.nombre as 'nombItem', IF(r.hasta < curdate(), '#232323', IF(r.estado = 2, 'red',i.color)) as 'color', concat(e.nombre, ' ' , e.apellido) as 'nomb' , iFNULL(re.nombre, '') as 'responsable' "
                . "from reserva r "
                . "INNER JOIN Item_reserva i on i.iditem = r.item "
                . "LEFT JOIN empleado e on e.idempleado = r.solicitante "
                . "LEFT JOIN responsable_reserva re on re.idresponsable = r.idresponsable "
                . "Where r.hasta > concat(YEAR(curdate()),'-',MONTH(curdate())-1,'-01') and r.estado in ($estado)  "
                . "union "
                . "select r.*, i.nombre as 'nombItem', IF(r.estado = 0, 'grey', IF(r.estado = 2, 'red',i.color)) as 'color', concat(e.nombre, ' ' , e.apellido) as 'nomb' , iFNULL(re.nombre, '') as 'responsable' "
                . "from reserva r "
                . "INNER JOIN Item_reserva i on i.iditem = r.item "
                . "LEFT JOIN empleado e on e.idempleado = r.solicitante "
                . "LEFT JOIN responsable_reserva re on re.idresponsable = r.idresponsable "
                . "Where r.hasta > concat(YEAR(curdate()),'-',MONTH(curdate())-3,'-01') and r.solicitante = $iduser and r.estado in(0,2)";
        //var_dump($sql);
        return ejecutarConsulta($sql);
    }

    function listarTodo($estado, $solicitante,$start,$end) {
        $sql = "select r.*, i.nombre as 'nombItem', IF(r.hasta < curdate(), '#232323', IF(r.estado = 2, 'red' , IF(r.estado = 3, 'red' , i.color))) as 'color', concat(e.nombre, ' ' , e.apellido) as 'nomb' "
                . ", iFNULL(re.nombre, '') as 'responsable' "
                . "from reserva r "
                . "INNER JOIN Item_reserva i on i.iditem = r.item "
                . "LEFT JOIN empleado e on e.idempleado = r.solicitante "
                . "LEFT JOIN responsable_reserva re on re.idresponsable = r.idresponsable "
                . "Where r.hasta BETWEEN CAST('$start' AS DATE) AND CAST('$end' AS DATE) and r.estado in ($estado) ";

        if ($solicitante != 0) {
            $sql .= " and r.solicitante = $solicitante";
        }
        //var_dump($sql);
        return ejecutarConsulta($sql);
    }

    function listarTodoDashboard($start,$end) {
        $sql = "select r.*, i.nombre as 'nombItem', IF(r.hasta < curdate(), '#232323', IF(r.estado = 2, 'red' , IF(r.estado = 3, 'red' , i.color))) as 'color', concat(e.nombre, ' ' , e.apellido) as 'nomb' "
                . ", iFNULL(re.nombre, '') as 'responsable' "
                . "from reserva r "
                . "INNER JOIN Item_reserva i on i.iditem = r.item "
                . "LEFT JOIN empleado e on e.idempleado = r.solicitante "
                . "LEFT JOIN responsable_reserva re on re.idresponsable = r.idresponsable "
                . "Where r.hasta BETWEEN CAST('$start' AS DATE) AND CAST('$end' AS DATE) and r.estado in (1) "
                . "order by r.desde, r.horadesde";

        return ejecutarConsulta($sql);
    }

    function notifica() {
        $sql = "select r.*, i.nombre as 'nombItem', i.color, concat(e.nombre, ' ' , e.apellido) as 'nomb' , iFNULL(re.nombre, '') as 'responsable' "
                . "from reserva r "
                . "INNER JOIN Item_reserva i on i.iditem = r.item "
                . "LEFT JOIN empleado e on e.idempleado = r.solicitante "
                . "LEFT JOIN responsable_reserva re on re.idresponsable = r.idresponsable "
                . "Where r.hasta > concat(YEAR(curdate()),'-',MONTH(curdate())-3,'-01') and estado =0 ";
        return ejecutarConsulta($sql);
    }

    function listarAdmin($rechazado) {
        if ($rechazado !== false) {
            $sql = "select r.*, i.nombre as 'nombItem',  IF(r.hasta < curdate(), '#232323', IF(r.estado = 0, 'grey', i.color)) as 'color', concat(e.nombre, ' ' , e.apellido) as 'nomb', iFNULL(re.nombre, '') as 'responsable' "
                    . "from reserva r "
                    . "INNER JOIN Item_reserva i on i.iditem = r.item "
                    . "LEFT JOIN empleado e on e.idempleado = r.solicitante "
                    . "LEFT JOIN responsable_reserva re on re.idresponsable = r.idresponsable "
                    . "Where r.hasta > concat(YEAR(curdate()),'-',MONTH(curdate())-3,'-01') and r.estado <> 2 ";
        } else {
            $sql = "select r.*, i.nombre as 'nombItem',  IF(r.hasta < curdate(), '#232323', IF(r.estado = 2, 'red', i.color)) as 'color', concat(e.nombre, ' ' , e.apellido) as 'nomb', iFNULL(re.nombre, '') as 'responsable' "
                    . "from reserva r "
                    . "INNER JOIN Item_reserva i on i.iditem = r.item "
                    . "LEFT JOIN empleado e on e.idempleado = r.solicitante "
                    . "LEFT JOIN responsable_reserva re on re.idresponsable = r.idresponsable "
                    . "Where r.hasta > concat(YEAR(curdate()),'-',MONTH(curdate())-3,'-01') and r.estado = 2 ";
        }
        return ejecutarConsulta($sql);
    }

    function verifica($item, $desde, $horadesde, $responsable) {
        $sql = "SELECT * FROM `reserva` "
                . "Where item = $item "
                . "and idresponsable = $responsable "
                . "and ('$desde' BETWEEN desde and hasta) "
                . "and (('$horadesde' >= horadesde and '$horadesde' <= horahasta)) and estado not in (2,3) "
                . "order by horadesde asc limit 1 ";


        $rspta = ejecutarConsultaSimpleFila($sql);
        json_encode($rspta);
        return $rspta;
    }

    function eliminar($idreserva, $comentario, $user) {

        $sql = "UPDATE `reserva` SET "
                . "`iduser`= $user,"
                . "`estado`= 2 ,"
                . "`comentario`= '$comentario' ,"
                . "`update_time`= CURRENT_TIMESTAMP,"
                . "`update_user`= $user "
                . "WHERE `idreserva`= $idreserva";

        return ejecutarConsulta($sql);

        //$sql = "DELETE FROM reserva WHERE idreserva = $idreserva";
        //return ejecutarConsulta($sql);
    }

    function anular($idreserva, $comentario, $user) {

        $sql = "UPDATE `reserva` SET "
                . "`iduser`= $user,"
                . "`estado`= 3 ,"
                . "`comentario`= '$comentario',"
                . "`update_time`= CURRENT_TIMESTAMP,"
                . "`update_user`= $user "
                . "WHERE `idreserva`= $idreserva";
        //var_dump($sql);
        return ejecutarConsulta($sql);

        //$sql = "DELETE FROM reserva WHERE idreserva = $idreserva";
        //return ejecutarConsulta($sql);
    }

    public function EnviarMail($emailfrom, $fromName, $username, $pass, $asunto, $mensaje, $idempleado) {
        $Mailer = new PHPMailer();
        $Mailer->isSMTP();
        $Mailer->CharSet = 'UTF-8';

        $Mailer->Port = 587;
        $Mailer->SMTPAuth = true;
        $Mailer->SMTPSecure = "tls";
        $Mailer->SMTPDebug = 0;
        $Mailer->Debugoutput = 'html';
        $Mailer->Host = "mail.fabrimetalsa.cl";
        $Mailer->Username = "notificaciones@fabrimetalsa.cl";
        $Mailer->Password = "*RUJQtbV!wK*";

        $Mailer->From = "notificaciones@fabrimetalsa.cl"; //"emergencia@fabrimetal.cl";


        $Mailer->FromName = $fromName; //"Sistema de solicitud de presupuestos";
        $Mailer->Subject = $asunto; //"Solicitud Informacion - Sol. Presupuesto N° " . $resp["idpresupuesto"];
        $Mailer->msgHTML($mensaje);


        $em = new Empleado();
        $respemail = $em->getMail($idempleado);

        while ($reg = $respemail->fetch_object()) {
            //var_dump($reg);
            $Mailer->addAddress($reg->email_corporativo, $reg->nombre);
        }
        //DIRECCION DE PRUEBA
        //$Mailer->addAddress("omaldonado@fabrimetal.cl",'');

        if (!$Mailer->send()) {
            echo "Error al enviar correo: " . $Mailer->ErrorInfo . "<br>";
        } else {
            echo "Correo enviado exitosamente<br>";
        }
    }

    public function EnviarMailresponsable($emailfrom, $fromName, $username, $pass, $asunto, $mensaje, $idresponsable) {
        $Mailer = new PHPMailer();
        $Mailer->isSMTP();
        $Mailer->CharSet = 'UTF-8';

        $Mailer->Port = 587;
        $Mailer->SMTPAuth = true;
        $Mailer->SMTPSecure = "tls";
        $Mailer->SMTPDebug = 0;
        $Mailer->Debugoutput = 'html';
        $Mailer->Host = "mail.fabrimetalsa.cl";
        $Mailer->Username = "notificaciones@fabrimetalsa.cl";
        $Mailer->Password = "*RUJQtbV!wK*";

        $Mailer->From = "notificaciones@fabrimetalsa.cl"; //"emergencia@fabrimetal.cl";
        $Mailer->FromName = $fromName; //"Sistema de solicitud de presupuestos";
        $Mailer->Subject = $asunto; //"Solicitud Informacion - Sol. Presupuesto N° " . $resp["idpresupuesto"];
        $Mailer->msgHTML($mensaje);

        $res = new Responsable();
        $respemail = $res->Mostrar($idresponsable);
        $Mailer->addAddress($respemail['emailEmpresa'], $respemail['nombre']);

        //DIRECCION DE PRUEBA
        //$Mailer->addAddress("omaldonado@fabrimetal.cl",'');

        if (!$Mailer->send()) {
            echo "Error al enviar correo: " . $Mailer->ErrorInfo . "<br>";
        } else {
            echo "Correo enviado exitosamente<br>";
        }
    }

}
