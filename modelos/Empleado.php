<?php 

require "../config/conexion.php";

	Class Empleado{
		//Constructor para instancias
		public function __construct(){

		}

		public function insertar($nombre,$apellido,$tipo_documento,$num_documento,$fecha_nac,$direccion,$movil, $residencial,$email,$imagen, $iduser, $idcargo, $idcomunas, $idregiones, $idoficina_departamento){
			$sql="INSERT INTO empleado (nombre, apellido, tipo_documento, num_documento, fecha_nac, direccion, movil, residencial, email, imagen, condicion, create_user, idcargo, idcomunas, idregiones, idoficina_departamento) VALUES ('$nombre', '$apellido', '$tipo_documento', '$num_documento', '$fecha_nac','$direccion', '$movil', '$residencial', '$email', '$imagen',1, '$iduser', '$idcargo', '$idcomunas', '$idregiones', '$idoficina_departamento')";
			return ejecutarConsulta($sql);
		}

		public function editar($idempleado,$nombre,$apellido,$tipo_documento,$num_documento,$fecha_nac,$direccion,$movil, $residencial,$email,$imagen, $iduser, $idcargo, $idcomunas, $idprovincias, $idregiones, $idoficina_departamento){
			$sql="UPDATE empleado SET nombre='$nombre', apellido='$apellido', tipo_documento='$tipo_documento', num_documento='$num_documento', direccion='$direccion', movil='$movil', residencial='$residencial', email='$email', fecha_nac='$fecha_nac', imagen='$imagen' , updated_user='$iduser', idcargo='$idcargo', idcomunas='$idcomunas', idprovincias='$idprovincias', idregiones='$idregiones', idoficina_departamento='$idoficina_departamento', updated_time=CURRENT_TIMESTAMP WHERE idempleado='$idempleado'";
			return ejecutarConsulta($sql);
		}

		public function desactivar($idempleado){
			$sql="UPDATE empleado SET condicion='0' WHERE idempleado='$idempleado'";
			return ejecutarConsulta($sql);
		}

		public function activar($idempleado){
			$sql="UPDATE empleado SET condicion='1' WHERE idempleado='$idempleado'";
			return ejecutarConsulta($sql);
		}

		public function mostrar($idempleado){
			$sql="SELECT e.*, w.idoficinas, w.iddepartamento FROM empleado e INNER JOIN oficina_departamento w ON w.idoficina_departamento = e.idoficina_departamento INNER JOIN oficinas o ON w.idoficinas = o.idoficinas INNER JOIN departamento d ON w.iddepartamento = d.iddepartamento INNER JOIN cargos c ON e.idcargo = c.idcargos WHERE e.idempleado='$idempleado'";
			return ejecutarConsultaSimpleFila($sql);
		}

		public function listar(){
			$sql="SELECT e.idempleado, e.condicion, e.nombre, e.apellido, e.tipo_documento, e.num_documento, e.movil, e.email, c.nombre AS cargo, d.nombre AS departamento, o.nombre AS oficinas FROM empleado e INNER JOIN oficina_departamento w ON w.idoficina_departamento = e.idoficina_departamento INNER JOIN oficinas o ON w.idoficinas = o.idoficinas INNER JOIN departamento d ON w.iddepartamento = d.iddepartamento INNER JOIN cargos c ON e.idcargo = c.idcargos";
			return ejecutarConsulta($sql);
		}

		public function listar_departamento($iddepartamento){
			$sql="SELECT e.nombre, e.apellido, e.tipo_documento, e.num_documento, e.movil, e.email, c.nombre AS cargo, o.nombre AS oficinas FROM empleado e INNER JOIN oficina_departamento w ON w.idoficina_departamento = e.idoficina_departamento INNER JOIN oficinas o ON w.idoficinas = o.idoficinas INNER JOIN departamento d ON w.iddepartamento = d.iddepartamento INNER JOIN cargos c ON e.idcargo = c.idcargos WHERE w.iddepartamento = '$iddepartamento'";
			return ejecutarConsulta($sql);			
		}

		public function listar_oficina($idoficinas){
			$sql="SELECT e.nombre, e.apellido, e.tipo_documento, e.num_documento, e.movil, e.email, c.nombre AS cargo, d.nombre AS departamento FROM empleado e INNER JOIN oficina_departamento w ON w.idoficina_departamento = e.idoficina_departamento INNER JOIN oficinas o ON w.idoficinas = o.idoficinas INNER JOIN departamento d ON w.iddepartamento = d.iddepartamento INNER JOIN cargos c ON e.idcargo = c.idcargos WHERE w.idoficinas = '$idoficinas'";
			return ejecutarConsulta($sql);
		}

		public function listar_cargo($idcargo){
			$sql="SELECT e.nombre, e.apellido, e.tipo_documento, e.num_documento, e.movil, e.email, d.nombre AS departamento, o.nombre AS oficinas FROM empleado e INNER JOIN oficina_departamento w ON w.idoficina_departamento = e.idoficina_departamento INNER JOIN oficinas o ON w.idoficinas = o.idoficinas INNER JOIN departamento d ON w.iddepartamento = d.iddepartamento INNER JOIN cargos c ON e.idcargo = c.idcargos WHERE e.idcargo = '$idcargo'";
			return ejecutarConsulta($sql);
		}

		public function selectempleado(){
			$sql="SELECT idempleado, nombre, apellido, num_documento FROM empleado order by nombre";
			return ejecutarConsulta($sql);
		}
                
                //obtiene los datos de un empleado enregando como parametro el Rut o numero de documento
                public function selectEmpleadoRut($rut){
                    $sql="SELECT e.*, w.idoficinas, w.iddepartamento FROM empleado e INNER JOIN oficina_departamento w ON w.idoficina_departamento = e.idoficina_departamento INNER JOIN oficinas o ON w.idoficinas = o.idoficinas INNER JOIN departamento d ON w.iddepartamento = d.iddepartamento INNER JOIN cargos c ON e.idcargo = c.idcargos WHERE e.num_documento='$rut'";
                    return ejecutarConsulta($sql);
                }
                
                //cuenta la cantidad de datos devueltos entregando como parametro el rut o numero de documento
                public function verificaempleado($rut){
                    $sql="SELECT count(*) as numFila FROM empleado WHERE num_documento='$rut'";
                    return ejecutarConsulta($sql);
                }
                
                //obtiene el correo corporativo del empleado
                public function getMail($idempleado){
                    $sql = "SELECT e.idempleado, concat(e.nombre, ' ', e.apellido) as 'nombre', e.email_corporativo, e.idcargo, c.nombre as 'cargo', e.idoficina_departamento, o.nombre as 'oficina' "
                            . "FROM `empleado` e "
                            . "INNER JOIN cargos c on c.idcargos = e.idcargo "
                            . "INNER JOIN oficina_departamento of on of.idoficina_departamento = e.idoficina_departamento "
                            . "INNER JOIN oficinas o on o.idoficinas = of.idoficinas "
                            . "WHERE e.idempleado = $idempleado";
                    return ejecutarConsulta($sql);
                }
                
	}
